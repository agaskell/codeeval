def evaluate a, b, o
  a = a.to_f
  b = b.to_f
  return a * b if o == "*"
  return a / b if o == "/"
  return a + b if o == "+"
end

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  line = line.split(" ")

  operators = line[0..(line.length / 2 - 1)]
  nums = line[(line.length / 2)..-1]

  while nums.length > 1
    o = operators.pop
    p = nums.shift
    nums[0] = evaluate p, nums.first, o
  end

  puts nums.first.to_i
end

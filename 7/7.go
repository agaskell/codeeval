package main

import "fmt"
import "log"
import "bufio"
import "os"
import s "strings"
import "strconv"

func Evaluate(a float64, b float64, c string) float64 {
  if c == "/" { return a / b }
  if c == "*" { return a * b }
  if c == "+" { return a + b }
  return 0.0
}

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  for scanner.Scan() {
    line := s.Split(scanner.Text(), " ")

    operators := line[:len(line) / 2]
    n := line[len(line) / 2:]

    nums := make([]float64, len(n))

    for i := range nums {
      nums[i], _ = strconv.ParseFloat(s.TrimSpace(n[i]), 64)
    }

    for len(nums) > 1 {
      // pop operator position
      l := len(operators) - 1

      // shift num
      p := nums[0]
      nums = nums[1:len(nums)]

      nums[0] = Evaluate(p, nums[0], operators[l])
      // icky to pop here but... it should work
      operators = operators[:l]
    }

    fmt.Println(int(nums[0]))
  }
}

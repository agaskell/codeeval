File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  s = line

  self_desc = true
  0.upto(s.length - 1) do |i|
    if s.count(i.to_s) != s[i].to_i
      self_desc = false
      break
    end
  end

  puts self_desc ? "1" : "0"

end

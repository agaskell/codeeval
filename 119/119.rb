File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  chains = Hash.new
  line.split(";").each do |m|
    a = m.split("-")
    chains[a[0]] = a[1]
  end

  result = "GOOD"
  move = "BEGIN"
  until chains.empty?

    if move.nil?
      result = "BAD"
      break
    end

    next_move = chains[move]

    if(chains.count == 1 && chains[move] != "END")
      result = "BAD"
    end
    chains.delete(move)

    move = next_move
  end

  puts result
end

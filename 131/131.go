package main

import "fmt"
import "log"
import "bufio"
import "os"
import s "strings"
import "strconv"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  for scanner.Scan() {
    line := s.Split(scanner.Text(), " ")

    value, pattern := line[0], line[1]

    i := s.Index(pattern, "+")

    if(i >= 0) {
      a, _ := strconv.Atoi(value[:i])
      b, _ := strconv.Atoi(value[i:])
      fmt.Println(a + b)

    } else {
      i = s.Index(pattern, "-")
      a, _ := strconv.Atoi(value[:i])
      b, _ := strconv.Atoi(value[i:])
      fmt.Println(a - b)
    }
  }
}

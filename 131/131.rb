File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  value, pattern = line.split(" ")

  index = pattern.index("+")

  if(index.nil?)
    index = pattern.index("-")
    value.insert(index, "-")
    a, b = value.split("-").map(&:to_i)
    puts a - b
  else
    value.insert(index, "+")
    a, b = value.split("+").map(&:to_i)
    puts a + b
  end

end

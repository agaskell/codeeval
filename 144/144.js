var fs  = require("fs");

var lookup = {2 : [2, 4, 8, 6],
              3 : [3, 9, 7, 1],
              4 : [4, 6],
              5 : [5],
              6 : [6],
              7 : [7, 9, 3, 1],
              8 : [8, 4, 2, 6],
              9 : [9, 1]};


fs.readFileSync(process.argv[2]).toString().split('\n').forEach(function (line) {
  if (line != "") {
    var numbers = line.split(" ");
    var a = parseInt(numbers[0]);
    var n = parseInt(numbers[1]);

    var ary = lookup[a];

    var all = Math.floor(n / ary.length);
    var rem = n % ary.length;

    var doComma = false;

    var out = "";
    for(var i = 0; i <= 9; i++) {
      if(doComma) {
        out += ", ";
      }
      doComma = true;

      var outputTemplate = i + ": ";
      var count = 0;

      for(var j = 0; j < ary.length; j++) {
        if(i == ary[j]) {
          count = all;
          if(j < rem) {
            count++;
          }
          break;
        }
      }

      out += outputTemplate + count;
    }
    console.log(out);

  }
});

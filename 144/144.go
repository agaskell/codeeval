package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strings"
import "strconv"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  m := map[int][]int {
    2 : []int { 2, 4, 8, 6 },
    3 : []int { 3, 9, 7, 1 },
    4 : []int { 4, 6 },
    5 : []int { 5 },
    6 : []int { 6 },
    7 : []int { 7, 9, 3, 1 },
    8 : []int { 8, 4, 2, 6 },
    9 : []int { 9, 1 },
  }

  for scanner.Scan() {
    numbers := strings.Split(scanner.Text(), " ")

    a, _ := strconv.Atoi(numbers[0])
    n, _ := strconv.Atoi(numbers[1])
    ary := m[a]

    all := n / len(ary)
    rem := n % len(ary)

    doComma := false
    for i := 0; i <= 9; i++ {
      if doComma {
        fmt.Print(", ")
      }
      doComma = true

      outputTemplate := strconv.Itoa(i) + ": "
      count := 0

      for j := range ary {
        if i == ary[j] {
          count = all
          if(j < rem) {
            count++
          }
          break
        }
      }

      fmt.Print(outputTemplate + strconv.Itoa(count))
    }

    fmt.Println("")
  }
}

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  a, n = line.split(" ").map(&:to_i)

  lookup = {2 => [2, 4, 8, 6],
            3 => [3, 9, 7, 1],
            4 => [4, 6],
            5 => [5],
            6 => [6],
            7 => [7, 9, 3, 1],
            8 => [8, 4, 2, 6],
            9 => [9, 1]}

  ary = lookup[a]

  #all digits will have this many
  all = n / ary.length

  #remainders
  rem = n % ary.length

  hash = Hash[ary.map {|item| [item, all]}]

  0.upto(rem - 1) { |i| hash[ary[i]] += 1 }

  0.upto(9) do |i|
    v = (hash.keys.include? i) ? hash[i] : 0
    print ", " if(i > 0)
    print "#{i}: #{v}"
  end

  puts
end

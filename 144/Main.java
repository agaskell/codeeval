import java.io.*;
import java.util.*;

public class Main  {
  private static Map<Long, Integer[]> map = new Hashtable<Long, Integer[]>();

  public static void main (String[] args) throws FileNotFoundException, IOException {
    File file = new File(args[0]);
    BufferedReader in = new BufferedReader(new FileReader(file));

    map.put((long)2, new Integer[] { 2, 4, 8, 6 });
    map.put((long)3, new Integer[] { 3, 9, 7, 1 });
    map.put((long)4, new Integer[] { 4, 6 });
    map.put((long)5, new Integer[] { 5 });
    map.put((long)6, new Integer[] { 6 });
    map.put((long)7, new Integer[] { 7, 9, 3, 1 });
    map.put((long)8, new Integer[] { 8, 4, 2, 6});
    map.put((long)9, new Integer[] { 9, 1 });

    String line;
    while ((line = in.readLine()) != null) {
      String[] numbers = line.split(" ");

      long a = Long.parseLong(numbers[0]);
      long n = Long.parseLong(numbers[1]);

      Integer[] ary = map.get(a);

      long all = n / ary.length;
      long rem = n % ary.length;

      boolean doComma = false;
      String out = "";

      for(int i = 0; i <= 9; i++) {
        if(doComma) {
          out += ", ";
        }
        doComma = true;

        String outputTemplate = i + ": ";
        long count = 0;

        for(int j = 0; j < ary.length; j++) {
          if(i == ary[j]) {
            count = all;
            if(j < rem) {
              count++;
            }
            break;
          }
        }
        out += outputTemplate + count;
      }
      System.out.println(out);
    }
  }
}

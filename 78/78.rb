def squares_good? board, total
  size = board.length
  sqrt = Math.sqrt(size).to_i

   #length is 4 or 9
  (0...size).step(sqrt).each do |i|
    (0...size).step(sqrt).each do |j|

      sum = 0
      (i...(i + sqrt)).each do |k|
        (j...(j + sqrt)).each do |l|
          sum += board[l][k]
        end

      end
      unless sum == total
        return false
      end
    end
  end

  true
end

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?
  size, ary = line.split(';')

  size = size.to_i
  total = 1.upto(size).inject(:+)

  valid = true
  board = ary.split(",").map(&:to_i).each_slice(size).to_a
  board.each_with_index do |r, i|
    #rows
    unless r.inject(:+) == total
      valid = false
      break
    end

    #columns
    sum = 0
    0.upto(size - 1) do |j|
      sum += board[i][j]
    end
    unless sum == total
      valid = false
      break
    end
  end

  unless valid
    puts "False"
    next
  end

  valid = squares_good? board, total
  puts valid ? "True" : "False"
end

package main

import "fmt"
import "log"
import "bufio"
import "os"
import s "strings"
import "strconv"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  for scanner.Scan() {
    line := s.Split(scanner.Text(), ":")

    list := s.Split(line[0], " ")

    swaps := s.Split(line[1], ",")

    for _, swap := range swaps {
      elements := s.Split(swap, "-")

      a, _ := strconv.Atoi(s.TrimSpace(elements[0]))
      b, _ := strconv.Atoi(s.TrimSpace(elements[1]))

      list[a], list[b] = list[b], list[a]
    }

    fmt.Println(s.Join(list, " "))
  }
}

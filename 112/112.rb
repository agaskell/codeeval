File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?
  list, swaps = line.split(":")

  list = list.split(" ").map(&:to_i)

  swaps.split(",").map { |k| k.split("-").map(&:to_i)}.each do |s|
    list[s[0]], list[s[1]] = list[s[1]], list[s[0]]
  end

  puts list.join(" ")
end

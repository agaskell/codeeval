package main

import "fmt"
import "log"
import "bufio"
import "os"
import s "strings"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)


  for scanner.Scan() {
    ary := s.Split(scanner.Text(), ",")
    a := make([]string, 0)

    for i := range ary {
      found := false
      for j := range a {
        if a[j] == ary[i] {
          found = true
        }
      }

      if !found {
        a = append(a, ary[i])
      }
    }

    fmt.Println(s.Join(a, ","))

  }
}

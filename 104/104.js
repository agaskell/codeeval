var fs  = require("fs");

var lookup = {
  "zero" : "0",
  "one" : "1",
  "two" : "2",
  "three" : "3",
  "four" : "4",
  "five" : "5",
  "six" : "6",
  "seven" : "7",
  "eight" : "8",
  "nine" : "9",
};

fs.readFileSync(process.argv[2]).toString().split('\n').forEach(function (line) {
    if (line != "") {
      var ary = line.split(';');

      var s = "";
      for(var i = 0; i < ary.length; i++) {
        s += lookup[ary[i]]
      }

      console.log(s);
    }
});

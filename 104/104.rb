File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  s = line.split(";")

  lookup = {
    "zero" => "0",
    "one" => "1",
    "two" => "2",
    "three" => "3",
    "four" => "4",
    "five" => "5",
    "six" => "6",
    "seven" => "7",
    "eight" => "8",
    "nine" => "9"
  }

  n = ""
  0.upto(s.length - 1) do |i|
    n += lookup[s[i]]
  end

  puts n
end

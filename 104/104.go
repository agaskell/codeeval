package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strings"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  lookup := map[string]string {
    "zero" : "0",
    "one" : "1",
    "two" : "2",
    "three" : "3",
    "four" : "4",
    "five" : "5",
    "six" : "6",
    "seven" : "7",
    "eight" : "8",
    "nine" : "9",
  }

  for scanner.Scan() {

    s := strings.Split(scanner.Text(), ";")

    for i := range s {
      fmt.Print(lookup[s[i]])
    }

    fmt.Println("")
  }
}

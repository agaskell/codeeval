import sys
test_cases = open(sys.argv[1], 'r')
total = 0
for test in test_cases:
    # ignore test if it is an empty line
    # 'test' represents the test case, do something with it
    # ...
    # ...
    total += int(test)
test_cases.close()

print(total)

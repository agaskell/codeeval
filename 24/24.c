#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
  FILE *f = fopen(argv[1], "r");
  char line[1024];

  int total = 0;

  while (fgets(line, 1024, f)) {
    if (line[0] == '\n') {
        continue;
    }

    total += atoi(&line[0]);
  }

  printf("%d\n", total);
  return 0;
}

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?
  puts line.split(" ").sort {|a, b| a.to_f <=> b.to_f }.join(" ")
end

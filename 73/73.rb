def in_range? i
  i >= 1 && i <= 26
end

@rcount = 0

def do_count str
  if str.empty?
    @rcount += 1
    return
  end

  a = str[0, 2]
  do_count str[1..-1]

  if a.length == 2 && in_range?(a.to_i)
    do_count str[2..-1]
  end
end

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?
  @rcount = 0
  do_count line
  puts @rcount
end

package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strconv"

func DoCount(str string, count *int) {
  if(len(str) == 0) {
    *count++;
    return
  }

  DoCount(str[1:], count)
  if(len(str) >= 2){
    s := str[0:2]
    n, _ := strconv.Atoi(s)
    if(len(s) == 2 && n >= 1 && n <= 26) {
      DoCount(str[2:], count)
    }
  }
}

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  for scanner.Scan() {
    line := scanner.Text()
    count := 0
    DoCount(line, &count)
    fmt.Println(count)
  }
}

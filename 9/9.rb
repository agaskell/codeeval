@stack = Array.new

def push(i)
  @stack.push(i)
end

def pop
  @stack.pop
end

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  ary = line.split(" ")

  ary.each { |item| push(item) }

  i = 0

  ary = []
  v = pop
  while v != nil
    ary.push(v) if i % 2 == 0
    i += 1
    v = pop
  end

  puts ary.join(" ")
end

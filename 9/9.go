package main

import "fmt"
import "log"
import "bufio"
import "os"
import s "strings"
import "strconv"

func Push(m []int, i int) []int {
  m = append(m, i)
  return m
}

func Pop(m []int) (int, []int) {
  l := len(m) - 1
  //fmt.Println(m)

  n, m := m[l], m[:l]
  //fmt.Println(m)

  return n, m
}

func main() {
    file, err := os.Open(os.Args[1])
    if err != nil {
        log.Fatal(err)
    }
    defer file.Close()
    scanner := bufio.NewScanner(file)


    for scanner.Scan() {
      a := s.Split(scanner.Text(), " ")
      m := make([]int, 0)

      for i := range a {
        b, _ := strconv.Atoi(a[i])
        m = Push(m, b)
      }

      for len(m) > 0 {
        v := 0
        v, m = Pop(m)
        fmt.Print(v)
        fmt.Print(" ")
        if len(m) > 0 {
          _, m = Pop(m)
        }
      }

      fmt.Println("")
    }
}

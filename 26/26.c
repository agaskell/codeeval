#include <sys/stat.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
  struct stat st;
  stat(argv[1], &st);
  printf("%lld\n", st.st_size);
  return 0;
}

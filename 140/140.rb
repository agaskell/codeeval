File.open(ARGV[0]).each_line do |line|
  next if line.empty?

  values, keys = line.split(";")
  values = values.split(" ")
  keys = keys.split(" ").map(&:to_i)

  missing = nil
  1.upto(values.length) do |i|
    unless keys.include? i
      keys.push i
      break
    end
  end

  puts Hash[keys.map.with_index { |x, i| [x, i] }.sort].values.map { |v|
    values[v]
  }.join(" ")
end

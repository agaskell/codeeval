def palindrome?(n)
  s = n.to_s
  mid = s.length / 2

  palin = true
  for i in 0..mid
    palin = false unless s[i] == s[i * -1 -1]
    break unless palin
  end
  palin
end


File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  one, two = line.split(" ").map(&:to_i)

  count = 0
  (one..two).each do |i|
    (i..two).each do |j|
      palins = 0
      (i..j).each do |k|
        palins += 1 if palindrome? k
      end
      count += 1 if palins % 2 == 0
    end
  end

  puts count
end

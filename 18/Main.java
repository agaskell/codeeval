import java.io.*;

public class Main  {
  public static void main (String[] args) throws FileNotFoundException, IOException {
    File file = new File(args[0]);
    BufferedReader in = new BufferedReader(new FileReader(file));
    String line;
    while ((line = in.readLine()) != null) {
      String[] ary = line.split(",");

      int n1 = Integer.parseInt(ary[0]);
      int n2 = Integer.parseInt(ary[1]);

      int i = 1;
      while(n2 * i < n1) { i++; }
      System.out.println(i * n2);
    }
  }
}

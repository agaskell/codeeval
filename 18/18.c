#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
  FILE *f = fopen(argv[1], "r");
  char line[1024];

  while (fgets(line, 1024, f)) {
    if (line[0] == '\n') {
        continue;
    }

    char *tok = strtok(&line[0], ",");

    int n1 = atoi(tok);
    tok = strtok(NULL, " ");
    int n2 = atoi(tok);

    int i = 1;

    while (n2 * i < n1) {
      i++;
    }

    printf("%d\n", i * n2);
  }

  return 0;
}

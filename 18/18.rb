File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  nums = line.split(',').map(&:to_i)

  i = 1

  while nums.last * i < nums.first
    i += 1
  end

  puts i * nums.last
end

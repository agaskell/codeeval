package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strings"
import "strconv"

func main() {
    file, err := os.Open(os.Args[1])
    if err != nil {
        log.Fatal(err)
    }
    defer file.Close()
    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        //'scanner.Text()' represents the test case, do something with it
        s := strings.Split(scanner.Text(), ",")

        x, _ := strconv.Atoi(s[0])
        n, _ := strconv.Atoi(s[1])

        i := 1

        for n * i < x {
          i++
        }

        fmt.Println(i * n);
    }
}

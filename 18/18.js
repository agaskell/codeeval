var fs  = require("fs");

fs.readFileSync(process.argv[2]).toString().split('\n').forEach(function (line) {
    if (line != "") {
      var ary = line.split(',');
      var n1 = parseInt(ary[0]);
      var n2 = parseInt(ary[1]);

      var i = 1;
      while(n2 * i < n1) {
        i++;
      }

      console.log(i * n2);
    }
});

package main

import "fmt"
import "log"
import "bufio"
import "os"
import s "strings"
import "strconv"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    level := s.Split(scanner.Text(), ";")
    size := s.Split(level[0], ",")

    rows, _ := strconv.Atoi(size[0])
    columns, _ := strconv.Atoi(size[1])
    columns = columns

    b := s.Split(level[1], "")

    board := make([][]string, rows)

    for i := 0; i < rows; i++ {
      start := i * columns
      board[i] = b[start:start + columns]
    }

    for i := 0; i < rows; i++ {
      for j := 0; j < columns; j++ {
        if board[i][j] == "*" {
          fmt.Print("*")
          continue
        }
        count := 0
        checkTop := i > 0
        checkRight := j < columns - 1
        checkBottom := i < rows - 1
        checkLeft := j > 0
        if checkLeft && checkTop && board[i - 1][j - 1] == "*" { count++ }
        if checkTop && board[i - 1][j] == "*" { count++ }
        if checkRight && checkTop && board[i - 1][j + 1] == "*" { count++ }
        if checkRight && board[i][j + 1] == "*" { count++ }
        if checkLeft && checkBottom && board[i + 1][j - 1] == "*" { count++ }
        if checkBottom && board[i + 1][j] == "*" { count++ }
        if checkRight && checkBottom && board[i + 1][j + 1] == "*" { count++ }
        if checkLeft && board[i][j - 1] == "*" { count++ }

        fmt.Print(count)
      }
    }

    fmt.Println("")
  }
}

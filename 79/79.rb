File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  size, b = line.split(";")
  rows, columns = size.split(",").map(&:to_i)
  b = b.split ""

  board = []
  0.upto(rows - 1) do |i|
    board << b.take(columns)
    b = b.drop(columns)
  end

  board.each_with_index do |row, i|
    row.each_with_index do |cell, j|
      next if cell == "*"
      count = 0
      check_top = i > 0
      check_right = j < columns - 1
      check_bottom = i < rows - 1
      check_left = j > 0

      count += 1 if check_left   && check_top && board[i - 1][j - 1] == "*"
      count += 1 if check_top    && board[i - 1][j] == "*"
      count += 1 if check_right  && check_top && board[i - 1][j + 1] == "*"
      count += 1 if check_right  && board[i][j + 1] == "*"

      count += 1 if check_left   && check_bottom && board[i + 1][j - 1] == "*"
      count += 1 if check_bottom && board[i + 1][j] == "*"
      count += 1 if check_right  && check_bottom && board[i + 1][j + 1] == "*"
      count += 1 if check_left   && board[i][j - 1] == "*"

      board[i][j] = count
    end
  end

  puts board.flatten.join

end

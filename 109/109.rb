class Point < Struct.new(:x, :y)
end

class Line
  attr_reader :name, :point1, :point2
  def initialize(line)
    @name = line.partition(":").first.to_i

    coords = line.split(",")

    @point1 = Point.new(coords[0].partition("([").last.to_f, coords[1].partition("]").first.to_f)
    @point2 = Point.new(coords[2].partition("[").last.to_f, coords[3].partition("])").first.to_f)
  end
end

class Lines < Array
  def calculate_intersections
    intersections = Hash.new(0)

    1.upto(self.length - 2) do |i|
      (i + 1).upto(self.length - 1) do |j|
        if(lines_intersect?(self[i], self[j]))
          intersections[self[i]] += 1
          intersections[self[j]] += 1
        end
      end
    end

    crud = intersections.sort_by { |k, v| v }

    unless intersections.empty?
      self.delete(crud.last[0])
      calculate_intersections
    end
  end
end

def subtract_points(point1, point2)
  return Point.new(point1.x - point2.x, point1.y - point2.y)
end

def cross_product(point1, point2)
  return point1.x * point2.y - point1.y * point2.x;
end

def lines_intersect?(line1, line2)
  r = subtract_points(line1.point2, line1.point1)
  s = subtract_points(line2.point2, line2.point1)

  numerator = cross_product(subtract_points(line2.point1, line1.point1), r)
  denominator = cross_product(r, s)

  return false if denominator == 0

  u = numerator / denominator
  t = cross_product(subtract_points(line2.point1, line1.point1), s) / denominator

  return (t >= 0) && (t <= 1) && (u >= 0) && (u <= 1)
end

lines = Lines.new

File.open(ARGV[0]).each_line do |line|
  next if line.empty?
  l = Line.new(line)
  lines.push(l)
end

lines.sort! { |a, b| a.name <=> b.name }

lines.calculate_intersections
lines.each { |l| puts l.name }

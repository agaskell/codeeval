require 'prime'

primes = Hash.new

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  i = line.to_i

  current_list = []
  2.upto(i - 1) do |j|
    primes[j] = Prime.prime?(j) if primes[j].nil?
    current_list << j if primes[j]
  end

  puts current_list.join(",")
end

package main

import "fmt"
import "log"
import "bufio"
import "os"
import s "strings"
import "strconv"
import "math/big"

const yes = 1
const no = 2


func main() {


  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  primes := make(map[int]int)

  for scanner.Scan() {
    i, _ := strconv.Atoi(scanner.Text())

    l := make([]string, 0)

    for k := 2; k < i; k++ {
      if primes[k] == 0 {
        b := big.NewInt(int64(k))
        isPrime := b.ProbablyPrime(0)
        if isPrime {
          primes[k] = yes
        } else {
          primes[k] = no
        }
      }
      if primes[k] == yes {
        l = append(l, strconv.Itoa(k))
      }
    }
    fmt.Println(s.Join(l, ","))
  }


}

package main

import "fmt"
import "log"
import "bufio"
import "os"
//import "strings"
import "strconv"


func BuildRow(ary []int) []int {

  ret := make([]int, len(ary) + 1)
  ret[0] = 1

  for i := 1; i < len(ary) + 1; i++ {
    val := 0
    if i > 0 { val = ary[i - 1] }
    val2 := 0
    if i < len(ary) { val2 = ary[i] }
    ret[i] = val + val2
  }
  return ret
}

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    s, _ := strconv.Atoi(scanner.Text())

    arys := make([][]int, s);

    t := make([]int, 0);

    printSpace := false

    for i := 0; i < s; i++ {
      arys[i] = BuildRow(t);
      t = arys[i]

      for _, j := range t {
        if printSpace { fmt.Print(" ") }
        printSpace = true
        fmt.Print(j)
      }
    }
    fmt.Println("")
  }
}

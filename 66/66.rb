def build_row ary
  ret = [1]
  return ret if ary.nil?

  1.upto(ary.length) do |i|
    val = ary[i - 1]
    val = 0 if val.nil?

    val2 = ary[i]
    val2 = 0 if val2.nil?

    ret.push(val + val2)
  end

  ret
end

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?
  l = line.to_i

  arys = []
  0.upto(l - 1) do |i|
    arys << build_row(arys[i - 1])
  end

  puts arys.join(" ")
end

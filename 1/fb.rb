File.open(ARGV[0]).each_line do |line|
  next if line.empty?
  n1, n2, limit = line.split.map(&:to_i)

  1.upto(limit) do |i|
    out = i
    out = "F" if i % n1 == 0
    out = "B" if i % n2 == 0
    out = "FB" if i % n1 == 0 && i % n2 == 0
    print out.to_s + " "
  end

  puts ''
end

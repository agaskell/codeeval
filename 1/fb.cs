using System;
using System.IO;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        using (var reader = File.OpenText(args[0]))
        while (!reader.EndOfStream)
        {
            var line = reader.ReadLine();
            if (null == line)
                continue;

            var ary = line.Split(' ');
            var n1 = int.Parse(ary[0]);
            var n2 = int.Parse(ary[1]);
            var limit = int.Parse(ary[2]);

            var s = "";
            for(var i = 1; i <= limit; i++) {
              var o = i.ToString();
              if(i % n1 == 0) o = "F";
              if(i % n2 == 0) o = "B";
              if(i % n1 == 0 && i % n2 == 0) o = "FB";
              s += o + ' ';
            }

            Console.WriteLine(s);
        }
    }
}

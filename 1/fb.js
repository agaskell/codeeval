var fs  = require("fs");
fs.readFileSync(process.argv[2]).toString().split('\n').forEach(function (line) {
    if (line != "") {
      var ary = line.split(' ');
      var n1 = parseInt(ary[0]);
      var n2 = parseInt(ary[1]);
      var limit = parseInt(ary[2]);

      var s = '';
      for(var i = 1; i <= limit; i++) {
        var out = i;
        if(i % n1 === 0) out = "F"
        if(i % n2 === 0) out = "B"
        if(i % n1 === 0 && i % n2 === 0) out = "FB"
        s += out + ' ';
      }

      console.log(s);
    }
});

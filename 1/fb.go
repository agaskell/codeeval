package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strings"
import "strconv"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    s := strings.Split(scanner.Text(), " ")

    n1, _ := strconv.Atoi(s[0])
    n2, _ := strconv.Atoi(s[1])
    limit, _ := strconv.Atoi(s[2])

    for i := 1; i <= limit; i++ {
      o := strconv.Itoa(i)
      if i % n1 == 0 { o = "F" }
      if i % n2 == 0 { o = "B" }
      if i % n1 == 0 && i % n2 == 0 { o = "FB" }
      fmt.Print(o + " ")
    }
    fmt.Println("")
  }
}

import java.io.*;

public class Main  {
    public static void main (String[] args) throws FileNotFoundException, IOException {
    File file = new File(args[0]);
    BufferedReader in = new BufferedReader(new FileReader(file));
    String line;
    while ((line = in.readLine()) != null) {
        String[] lineArray = line.split(" ");
        if (lineArray.length > 0) {
          int n1 = Integer.parseInt(lineArray[0]);
          int n2 = Integer.parseInt(lineArray[1]);
          int limit = Integer.parseInt(lineArray[2]);

          String s = "";
          for(Integer i = 1; i <= limit; i++) {
            String o = i.toString();
            if(i % n1 == 0) o = "F";
            if(i % n2 == 0) o = "B";
            if(i % n1 == 0 && i % n2 == 0) o = "FB";
            s += o + ' ';
          }

          System.out.println(s);

        }
    }
  }
}

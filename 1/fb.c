#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
  FILE *f = fopen(argv[1], "r");
  char line[1024];

  while (fgets(line, 1024, f)) {
    if (line[0] == '\n') {
        continue;
    }

    char *tok = strtok(&line[0], " ");

    int f = atoi(tok);
    tok = strtok(NULL, " ");
    int b = atoi(tok);
    tok = strtok(NULL, " ");
    int limit = atoi(tok);

    for(int i = 1; i <= limit; i++) {
      if(i % f == 0 && i % b == 0) { printf("FB "); }
      else if(i % f == 0) { printf("F "); }
      else if(i % b == 0) { printf("B "); }
      else { printf("%d ", i); }
    }
    printf("\n");
  }

  return 0;
}

@map = {
  "0" => ["0"],
  "1" => ["1"],
  "2" => ["a", "b", "c"],
  "3" => ["d", "e", "f"],
  "4" => ["g", "h", "i"],
  "5" => ["j", "k", "l"],
  "6" => ["m", "n", "o"],
  "7" => ["p", "q", "r", "s"],
  "8" => ["t", "u", "v"],
  "9" => ["w", "x", "y", "z"]
}


def r ch, s, out
  #puts "'#{ch}'"
  unless ch.nil? || ch.empty?
    @map[ch].each do |i|
      #puts s[1..-1]
      #puts ch
      r(s[0], s[1..-1], out + i)
    end
  end
  if out.length == 7
    print "," if @printComma
    @printComma = true
    print out
  end
end

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  @printComma = false
  r line[0], line[1..-1], ""
  puts
end

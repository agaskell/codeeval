File.open(ARGV[0]).each_line do |line|
  next if line.empty?
  s = line.split(",")
  v = Hash[s.group_by {|x| x}
    .map { |k, v| [k, v.count]}]
    .sort_by { |k, v| v}.last

  puts (v[1].to_i >= s.length / 2) ? v[0] : "None"
end

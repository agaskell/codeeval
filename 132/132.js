var fs  = require("fs");
fs.readFileSync(process.argv[2]).toString().split('\n').forEach(function (line) {
    if (line !== "") {
      var nums = line.split(",");
      nums.sort();

      var length = nums.length;
      var half = Math.floor(length / 2);

      var count = 0;
      var current = nums[0];
      var valid = false;

      for(var i = 0; i < length; i++) {
        if(nums[i] === current) {
          count++;
          if(count === half) {
            valid = true;
            break;
          }
        } else {
          count = 0;
          current = nums[i];
        }
      }

      if(valid) {
        console.log(current);
      } else {
        console.log("None");
      }

    }
});

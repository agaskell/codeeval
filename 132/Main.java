import java.io.*;
import java.util.*;

public class Main  {
  public static void main (String[] args) throws FileNotFoundException, IOException {
    File file = new File(args[0]);
    BufferedReader in = new BufferedReader(new FileReader(file));
    String line;
    while ((line = in.readLine()) != null) {
      List<String> nums = new ArrayList<String>(Arrays.asList(line.split(",")));

      Collections.sort(nums);

      int length = nums.size();
      int half = length / 2;
      boolean valid = false;

      int count = 0;
      String current = nums.get(0);
      for(int i = 0; i < length; i++) {
        if(nums.get(i).equals(current)) {
          count++;
          if(count == half) {
            valid = true;
            break;
          }
        } else {
          count = 0;
          current = nums.get(i);
        }
      }

      if(valid) {
        System.out.println(current);
      } else {
        System.out.println("None");
      }
    }
  }
}

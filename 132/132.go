package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strings"
//import "strconv"
import "sort"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    nums := strings.Split(scanner.Text(), ",")
    //nums := make([]int, len(ary))

    //for i := range ary {
    //  nums[i], _ = strconv.Atoi(ary[i])
    //}

    sort.Strings(nums)

    l := len(nums) - 1
    half := l / 2

    count := 0
    current := nums[0]

    valid := false
    for i := range nums {
      if nums[i] == current {
        count++
        if count == half {
          valid = true
          break
        }
      } else {
        count = 0
        current = nums[i]
      }
    }

    if valid {
      fmt.Println(current)
    } else {
      fmt.Println("None")
    }
  }
}

import java.io.*;

public class Main  {
  public static void main (String[] args) throws FileNotFoundException, IOException {
    File file = new File(args[0]);
    BufferedReader in = new BufferedReader(new FileReader(file));
    String line;
    while ((line = in.readLine()) != null) {
      String s = "";
      for(int i = 0; i < line.length(); i++) {
        String u = line.substring(i, i + 1).toUpperCase();
        if(line.substring(i, i + 1).equals(u)) {
          s += u.toLowerCase();
        } else {
          s += u;
        }
      }
      System.out.println(s);
    }
  }
}

require 'prime'

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  0.upto(line.length - 1) do |i|
    if line[i] === line[i].upcase
      line[i] = line[i].downcase
    else
      line[i] = line[i].upcase
    end
  end

  puts line
end

package main

import "fmt"
import "log"
import "bufio"
import "os"
import s "strings"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    line := scanner.Text()

    o := ""
    for i := range line {
      char := string(line[i])
      u := s.ToUpper(char)
      if char == u {
        o += s.ToLower(char)
      } else {
        o += u
      }
    }

    fmt.Println(o)
  }
}

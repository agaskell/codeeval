package main

import "fmt"
import "log"
import "bufio"
import "os"
import s "strings"
import "strconv"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  for scanner.Scan() {
    line := s.Split(scanner.Text(), " ")

    count, current, s := 0, "", ""

    for i := range line {
      if line[i] == current {
        count++
      } else {
        if count != 0 {
          s += strconv.Itoa(count) + " " + current + " "
        }
        current = line[i]
        count = 1
      }

      if i == len(line) - 1 {
        s += strconv.Itoa(count) + " " + current
      }
    }
    fmt.Println(s)
  }
/*

ary = line.split(' ')

count = 0
current = nil
s = ''

ary.each_with_index do |c, i|
  if c == current
    count += 1
  else
    unless count == 0
      s += "#{count} #{current} "
    end
    current = c
    count = 1
  end

  s += "#{count} #{current} " if i == ary.length - 1
end

puts s.strip */

}

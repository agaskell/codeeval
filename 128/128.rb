File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?


  ary = line.split(' ')

  count = 0
  current = nil
  s = ''

  ary.each_with_index do |c, i|
    if c == current
      count += 1
    else
      unless count == 0
        s += "#{count} #{current} "
      end
      current = c
      count = 1
    end

    s += "#{count} #{current} " if i == ary.length - 1
  end

  puts s.strip

end

board = []

i, j = 255, 255

0.upto(i) do |a|
  ary = []
  0.upto(j) do |b|
    ary.push(0)
  end
  board.push(ary)
end

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  command = line.split(" ")

  if(command[0].start_with? "Set")
    command, target, value = command[0], command[1].to_i, command[2].to_i

    if("SetCol" == command)
      0.upto(j) {|v| board[target][v] = value}
    end
    if("SetRow" == command)
      0.upto(i) {|v| board[v][target] = value}
    end

  else
    command, target = command[0], command[1].to_i

    total = 0

    if("QueryCol" == command)
      0.upto(j) {|v| total += board[target][v];  }
    end
    if("QueryRow" == command)
      0.upto(i) {|v| total += board[v][target] }
    end

    puts total
  end

end

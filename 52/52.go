package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strconv"

func BuildSegment(num int, m map[int]string) string {
  o := ""

  if num >= 100 {
    v := num / 100
    o += m[v] + "Hundred"
    num -= v * 100
  }

  if num >= 20 {
    v := num / 10
    o += m[v * 10]
    num -= v * 10
  }

  if num < 20 {
    o += m[num]
  }
  return o
}

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  const million = 1000000
  const thousand = 1000

  m := map[int]string {
    0 : "",
    1 : "One",
    2 : "Two",
    3 : "Three",
    4 : "Four",
    5 : "Five",
    6 : "Six",
    7 : "Seven",
    8 : "Eight",
    9 : "Nine",
    10 : "Ten",

    11 : "Eleven",
    12 : "Twelve",
    13 : "Thirteen",
    14 : "Fourteen",
    15 : "Fifteen",
    16 : "Sixteen",
    17 : "Seventeen",
    18 : "Eighteen",
    19 : "Nineteen",

    20 : "Twenty",
    30 : "Thirty",
    40 : "Forty",
    50 : "Fifty",
    60 : "Sixty",
    70 : "Seventy",
    80 : "Eighty",
    90 : "Ninety",
  }

  for scanner.Scan() {
    num, _ := strconv.Atoi(scanner.Text())

    o := ""

    res := num / million

    if res > 0 {
      o += BuildSegment(res, m) + "Million"
      num -= million * res
    }

    res = num / thousand
    if res > 0 {
      o += BuildSegment(res, m) + "Thousand"
      num -= thousand * res
    }

    fmt.Println(o + BuildSegment(num, m) + "Dollars")
  }
}

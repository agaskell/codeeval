@map = {
  0 => "",
  1 => "One",
  2 => "Two",
  3 => "Three",
  4 => "Four",
  5 => "Five",
  6 => "Six",
  7 => "Seven",
  8 => "Eight",
  9 => "Nine",
  10 => "Ten",

  11 => "Eleven",
  12 => "Twelve",
  13 => "Thirteen",
  14 => "Fourteen",
  15 => "Fifteen",
  16 => "Sixteen",
  17 => "Seventeen",
  18 => "Eighteen",
  19 => "Nineteen",

  20 => "Twenty",
  30 => "Thirty",
  40 => "Forty",
  50 => "Fifty",
  60 => "Sixty",
  70 => "Seventy",
  80 => "Eighty",
  90 => "Ninety"
}

def build_segment num
  o = ''

  s = num.to_s.reverse

  if s.length == 3
    o += @map[s[2].to_i] + "Hundred"
  end

  if s.length >= 2
    v = (s[1] + s[0]).to_i
    if v < 20
      o += @map[v]
      return o
    else
      o += @map[(s[1] + "0").to_i]
    end
  end
  o += @map[s[0].to_i]
  o
end

@million = 1000000
@thousand = 1000

File.open(ARGV[0]).each_line do |line|
  next if line.empty?

  num = line.to_i

  res = num / @million

  o = ''
  if res > 0
    o += build_segment(res) + "Million"
    num -= @million * res
  end

  res = num / @thousand
  if res > 0
    o += build_segment(res) + "Thousand"
    num -= @thousand * res
  end

  puts o + build_segment(num) + "Dollars"
end

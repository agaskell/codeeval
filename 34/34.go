package main

import "fmt"
import "log"
import "bufio"
import "os"
import s "strings"
import "strconv"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  for scanner.Scan() {
    line := s.Split(scanner.Text(), ";")

    numsStringArray := s.Split(line[0], ",")
    nums := make([]int, len(numsStringArray));

    for i, _ := range nums {
      nums[i], _ = strconv.Atoi(numsStringArray[i])
    }

    target, _ := strconv.Atoi(line[1])

    out := make([][]int, 0)

    for i := range nums {
      for j := i; j < len(nums); j++ {
        r := nums[i] + nums[j]
        if r == target {
          out = append(out, []int{nums[i], nums[j]})
        }
        if r >= target {
          break
        }
      }
    }

    if len(out) == 0 {
      fmt.Println("NULL")
    } else {
      s := ""
      printSemiColon := false
      for i := range out {
        if printSemiColon { s += ";" }
        printSemiColon = true
        s += fmt.Sprintf("%d,%d", out[i][0], out[i][1])
      }
      fmt.Println(s)
    }
  }
}

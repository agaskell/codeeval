File.open(ARGV[0]).each_line do |line|
  next if line.strip.empty?

  values, target = line.strip.split(";")
  values = values.split(",").map(&:to_i)

  target = target.to_i

  matches = []
  0.upto(values.length - 1) do |i|
    (i + 1).upto(values.length - 1) do |j|
      matches.push(values[i].to_s + "," + values[j].to_s) if(values[i] + values[j] == target)
    end
  end

  if matches.empty?
    puts "NULL"
  else
    puts matches.join(";")
  end
end

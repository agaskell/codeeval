File.open(ARGV[0]).each_line do |line|
  next if line.empty?
  v = Hash[line.split(" ").map.with_index { |k, i| [k, [k.length, i]]}.sort_by {|k| [k[1][0] * -1, k[1][1] ]}]
  puts v.first[0]
end

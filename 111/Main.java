import java.io.*;

public class Main  {
  public static void main (String[] args) throws FileNotFoundException, IOException {
    File file = new File(args[0]);
    BufferedReader in = new BufferedReader(new FileReader(file));
    String line;
    while ((line = in.readLine()) != null) {
      String[] ary = line.split(" ");

      String longest = "";
      int length = 0;

      for(int i = 0; i < ary.length; i++) {
        if(ary[i].length() > length) {
          longest = ary[i];
          length = longest.length();
        }
      }

      System.out.println(longest);
    }
  }
}

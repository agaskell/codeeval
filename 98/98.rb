def parse_coords c
  s = c.index("(") + 1
  e = c.index(")")
  c[s...e].split(",").map(&:to_f)
end

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?
  center, radius, point = line.split(";")

  center_x, center_y = parse_coords center
  point_x, point_y = parse_coords point

  puts Math.sqrt((point_x - center_x) ** 2 + (point_y - center_y) ** 2) < radius.split(":").last.to_f
end

package main

import "fmt"
import "log"
import "bufio"
import "os"
import s "strings"
import "strconv"
import "math"

func ParseCoords(c string) (float64, float64) {
  start := s.Index(c, "(") + 1
  end := s.Index(c, ")")

  t := s.Split(c[start:end], ",")

  a, _ := strconv.ParseFloat(s.TrimSpace(t[0]), 64)
  b, _ := strconv.ParseFloat(s.TrimSpace(t[1]), 64)
  return a, b
}

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  for scanner.Scan() {
    line := s.Split(scanner.Text(), ";")
    center_x, center_y := ParseCoords(line[0])

    radius, _ := strconv.ParseFloat(s.TrimSpace(s.Split(line[1], ":")[1]), 64)
    point_x, point_y := ParseCoords(line[2])

    lhs := math.Sqrt(math.Pow(point_x - center_x, 2) + math.Pow(point_y - center_y, 2))
    fmt.Println(lhs < radius)
  }
}

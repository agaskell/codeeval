package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strings"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    ary := strings.Split(scanner.Text(), ",")
    o := "0"
    if(strings.HasSuffix(ary[0], ary[1])) {
      o = "1"
    }
    fmt.Println(o)
  }
}

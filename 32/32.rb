File.open(ARGV[0]).each_line do |line|
  next if line.strip.empty?

  l1, l2 = line.strip.split(",")

  puts (l1.end_with? l2) ? "1" : "0"
end

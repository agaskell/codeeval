def clean_rows ary
  ary.each { |r| ary.delete(r) if r.empty? }
end

File.open(ARGV[0]).each_line do |line|
  next if line.empty?
  r, c, numbers = line.split(';')

  c = c.to_i

  numbers = numbers.split(' ')
  rows = numbers.each_slice(c).to_a

  o = []

  until rows.empty?
    #ltr
    o << rows.first.shift until rows.first.empty?
    clean_rows(rows)

    #ttb
    break if rows.empty?
    rows.each { |r| o << r.pop }
    clean_rows(rows)

    #rtl
    break if rows.empty?
    o << rows.last.pop until rows.last.empty?
    clean_rows(rows)

    #bbt
    break if rows.empty?
    rows.reverse.each { |r| o << r.shift }
    clean_rows(rows)
  end

  puts o.join(" ")
end

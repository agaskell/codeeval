package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strings"
import "strconv"

func CleanRows(slicePtr *[][]string) {
  slice := *slicePtr
  for i := len(slice) - 1; i >= 0; i-- {
    if len(slice[i]) == 0 {
      *slicePtr = slice[:i+copy(slice[i:], slice[i+1:])]
    }
  }
}

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    s := strings.Split(scanner.Text(), ";")

    r, _ := strconv.Atoi(s[0])
    c, _ := strconv.Atoi(s[1])

    data := strings.Split(s[2], " ");

    rows := make([][]string, r)

    index := 0

    for i := 0; i < r; i++ {
      rows[i] = make([]string, c)
      for j := range rows[i] {
        rows[i][j] = data[index]
        index += 1
      }
    }

    o := make([]string, len(data))
    index = 0

    for len(rows) > 0 {
      for len(rows[0]) > 0 {
        o[index], rows[0] = rows[0][0], rows[0][1:]
        index++
      }
      CleanRows(&rows)

      if len(rows) == 0 { break }
      for i := range rows {
        if len(rows[i]) > 0 {
          o[index], rows[i] = rows[i][len(rows[i]) - 1], rows[i][:len(rows[i]) - 1]
          index++
        }
      }
      CleanRows(&rows)

      if len(rows) == 0 { break }
      last := len(rows) - 1
      for len(rows[last]) > 0 {
        o[index], rows[last] = rows[last][len(rows[last]) - 1], rows[last][:len(rows[last]) - 1]
        index++
      }
      CleanRows(&rows)

      if len(rows) == 0 { break }
      for i := len(rows) - 1; i >= 0; i-- {
        if len(rows[i]) > 0 {
          o[index], rows[i] = rows[i][0], rows[i][1:]
          index++
        }
      }
      CleanRows(&rows)
    }

    for i := range o {
      fmt.Print(o[i] + " ")
    }
    fmt.Println("");
  }
}

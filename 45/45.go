package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strconv"
import "math"

func IsPalindrome(n int) bool {
  s := strconv.Itoa(n)
  length := len(s)
  mid := length / 2

  isPalindrome := true
  for i := 0; i < mid; i++ {
    if s[i:i + 1] != s[length - i - 1:length - i] {
      isPalindrome = false
      break
    }
  }
  return isPalindrome
}

func Reverse(n int) int {
  result := 0
  for n > 0 {
    result += (n % 10) * int(math.Pow(10, math.Floor(math.Log10(float64(n)))))
    n /= 10
  }
  return result
}

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    n, _ := strconv.Atoi(scanner.Text())

    count := 0
    for !IsPalindrome(n) {
      n += Reverse(n)
      count++;
    }

    fmt.Printf("%d %d\n", count, n)
  }
}

def palindrome?(n)
  s = n.to_s
  mid = s.length / 2

  palin = true
  for i in 0..mid
    palin = false unless s[i] == s[i * -1 -1]
    break unless palin
  end
  palin
end

File.open(ARGV[0]).each_line do |line|
  next if line.empty?
  n = line.to_i

  count = 0
  until(palindrome? n)
    n += n.to_s.reverse.to_i
    count += 1
  end

  puts "#{count} #{n.to_s}"
end

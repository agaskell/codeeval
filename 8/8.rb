File.open(ARGV[0]).each_line do |line|
  next if line.empty?
  puts line.split.reverse.join(" ").strip
end

package main

import "fmt"
import "log"
import "bufio"
import "os"
import s "strings"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  for scanner.Scan() {
    line := s.Split(scanner.Text(), " ")

    l := len(line) - 1

    for i := l; i >= 0; i-- {
      fmt.Print(line[i] + " ")
    }

    fmt.Println("")
  }
}

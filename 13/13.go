package main

import "fmt"
import "log"
import "bufio"
import "os"
import s "strings"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  for scanner.Scan() {
    line := s.Split(scanner.Text(), ",")

    sentence := line[0]
    chars := s.TrimSpace(line[1])

    for _, c := range chars {
      sentence = s.Replace(sentence, string(c), "", -1);
    }

    fmt.Println(sentence);
  }
}

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?
  s1, replacement_values = line.split(",")
  replacement_values.strip!
  puts s1.tr(replacement_values, "").strip
end

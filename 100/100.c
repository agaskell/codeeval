#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
  FILE *f = fopen(argv[1], "r");
  char line[1024];

  while (fgets(line, 1024, f)) {
    if (line[0] == '\n') {
        continue;
    }
    int i = atoi(line) % 2 == 0;

    printf("%d\n", i);
  }

  return 0;
}

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  num = line.to_i
  perms = line.split("").map(&:to_i).permutation.sort.to_a.uniq.map do |e|
    e.map(&:to_s).inject(:+)
  end

  perms.map! {|e| e.to_i }

  n = perms.find {|i| i > num }

  while n.nil?
    n = perms.shift.to_s
    n = (n[0] + "0" + n[1..-1]).to_i
    n = nil if perms.include? n
  end

  puts n.inspect
end

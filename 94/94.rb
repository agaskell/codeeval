def evaluate s
  s = s.gsub("(", "").gsub(")", "")

  puts s


  until s.index("^").nil?
    a, b = s.split("^").map(&:to_f)
  end

  unless (capture = s[/[\*\/]/]).nil?
    a, b = s.split("*").map(&:to_f)
    a * b
  end
  unless s.index("/").nil?
    a, b = s.split("/").map(&:to_f)
    a / b
  end
  until (capture = s[/[\+\-]/]).nil?
    a, b = s.split(capture).map(&:to_f)
    return a - b if capture == "-"
    return a + b if capture == "+"
  end

  #unless s.index("-").nil?
#    a, b = s.split("-").map(&:to_f)
#    a - b
#  end


end

File.open(ARGV[0]).each_line do |line|
  next if line.empty?

  stack = []

  0.upto(line.length - 1) do |i|
    stack.push i if line[i] == "("

    if(line[i] == ")")
      j = stack.pop


      puts evaluate(line[j..i])
    end

  end

end

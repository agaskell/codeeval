def d(l1, l2)
  0.upto(l1.length - 1) do |i|
    current_char = l1[i]

    0.upto(l2.length) do |j|
      if l2[j] == current_char
        current_char + d(l1[(i+1)..-1], l2)
        return current_char
      end
    end

    return ""
  end
end

File.open(ARGV[0]).each_line do |line|
  next if line.empty?
  l1, l2 = line.split(';')

  puts d(l1, l2)
end

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
  FILE *f = fopen(argv[1], "r");
  char line[1024];

  while (fgets(line, 1024, f)) {
    if (line[0] == '\n') {
        continue;
    }
    printf("%d\n", (int)strtol(line, NULL, 16));
  }

  return 0;
}

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  a, b = line.split('|')

  a = a.split(" ").map(&:to_i)

  b = b.split(" ").map(&:to_i)


  s = ''
  a.each_with_index { |e, i| s += (e * b[i]).to_s + " " }

  puts s.strip
end

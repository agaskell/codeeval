package main

import "fmt"
import "log"
import "bufio"
import "os"
import s "strings"
import "strconv"

func main() {
    file, err := os.Open(os.Args[1])
    if err != nil {
        log.Fatal(err)
    }
    defer file.Close()
    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
      str := s.Split(scanner.Text(), "|")

      l1 := s.Split(s.TrimSpace(str[0]), " ")
      l2 := s.Split(s.TrimSpace(str[1]), " ")

      for i := range l1 {
        a, _ := strconv.Atoi(l1[i])
        b, _ := strconv.Atoi(l2[i])
        fmt.Print(a * b)
        fmt.Print(" ")
      }

      fmt.Println()
    }
}

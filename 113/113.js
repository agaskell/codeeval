var fs  = require("fs");

fs.readFileSync(process.argv[2]).toString().split('\n').forEach(function (line) {
  if (line != "") {

    var ary = line.split("|");

    var lhs = ary[0].trim().split(" ");
    var rhs = ary[1].trim().split(" ");

    var s = "";
    for(var i = 0; i < lhs.length; i++) {
      s += parseInt(lhs[i]) * parseInt(rhs[i]) + " ";
    }

    console.log(s);
  }
});

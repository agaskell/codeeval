package main

import "fmt"
import "log"
import "bufio"
import "os"
import s "strings"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  for scanner.Scan() {
    line := s.ToLower(scanner.Text())

    a := []string { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l",
    "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"}

    for _, i := range line {
      for j := range a {
        if i == rune(a[j][0]) {
          a = append(a[:j], a[j + 1:]...)
          break
        }
      }
    }

    if(len(a) == 0) {
      fmt.Println("NULL")
    } else {
      fmt.Println(s.Join(a, ""))
    }
  }
}

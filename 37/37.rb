File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  line = line.downcase

  a = ("a".."z").to_a

  0.upto(line.length - 1) do |i|
    a.delete(line[i])
  end

  result = "NULL"
  result = a.join unless a.empty?
  puts result
end

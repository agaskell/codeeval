class Item
  attr_accessor :id, :weight, :cost

  def initialize(line)
    @id = line[0].gsub("(", "").to_i
    @weight = line[1].to_f
    @cost = line[2].gsub("$", "").gsub(")", "").to_i
  end

  def efficiency
    @cost / @weight
  end
end

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  limit, i = line.split(":")
  limit = limit.to_i

  items = []

  i.strip.split(" ").map { |m| m.split(",")}.each do |line|
    item = Item.new(line)
    items << item if item.weight <= limit
  end

  items.sort! {|a, b| b.efficiency <=> a.efficiency }

  max = 0

  0.upto(items.length - 1) do |i|
      items.shift

  end

  items.shift
  items.each do |item|



  end


  puts ""
end

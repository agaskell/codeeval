File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  lhs, replacements = line.split(';')
  replacements = replacements.split(",")

  ary = []
  until replacements.empty?
    cv = replacements.shift
    rv = replacements.shift
    index = lhs.index(cv)

    until index.nil?
      ary << [rv, index]
      lhs.sub! cv, "X" * rv.length
      diff = rv.length - cv.length
      ary.each { |a| a[1] += diff if a.last > index }
      index = lhs.index(cv)
    end
  end

  until ary.empty?
    value, index = ary.shift
    lhs = lhs[0...index] + value + lhs[index + value.length..-1]
  end

  puts lhs
end

package main

import "fmt"
import "log"
import "bufio"
import "os"
import s "strings"

type ValuePair struct {
    value string
    index int
}

func BuildString(s string, count int) string {
  o := ""
  for i := 0; i < count; i++ {
    o += s;
  }
  return o;
}

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    line := s.Split(scanner.Text(), ";")
    lhs := line[0];
    replacements := s.Split(line[1], ",")

    ary := make([]ValuePair, 0)

    for i := 0; i < len(replacements); i+= 2 {
      cv := replacements[i]
      rv := replacements[i + 1]
      index := s.Index(lhs, cv)

      for index > -1 {
        vp := ValuePair { rv, index }

        lhs = lhs[:index] + BuildString("X", len(rv)) + lhs[index + len(cv):]

        ary = append(ary, vp)
        diff := len(rv) - len(cv)
        for j := range ary {
          if ary[j].index > index { ary[j].index += diff }
        }
        index = s.Index(lhs, cv)
      }
    }

    for i := range ary {
      kv := ary[i]
      lhs = lhs[:kv.index] + kv.value +
        lhs[kv.index + len(kv.value): len(lhs)];
    }

    fmt.Println(lhs)
  }
}

import java.io.*;
import java.util.*;

public class Main  {
  static class ValuePair {
    public String value;
    public int index;

    public ValuePair(String value, int index) {
      this.value = value;
      this.index = index;
    }

    public String toString() {
      return "value = " + value + "; index = " + index;
    }
  }

  private static String buildString(char c, int count) {
    StringBuilder sb = new StringBuilder();
    for(int i = 0; i < count; i++) {
      sb.append(c);
    }
    return sb.toString();
  }

  public static void main (String[] args) throws FileNotFoundException, IOException {
    File file = new File(args[0]);
    BufferedReader in = new BufferedReader(new FileReader(file));
    String line;

    while ((line = in.readLine()) != null) {
      String[] lineArray = line.split(";");
      String lhs = lineArray[0];
      String[] replacementPairs = lineArray[1].split(",");

      List<ValuePair> values = new ArrayList<ValuePair>();

      for(int i = 0; i < replacementPairs.length; i += 2) {
        String currentValue = replacementPairs[i];
        String replacementValue = replacementPairs[i + 1];

        int index = lhs.indexOf(currentValue);

        while(index > -1) {
          values.add(new ValuePair(replacementValue, index));

          lhs = lhs.substring(0, index) +
            buildString('X', replacementValue.length()) +
            lhs.substring(index + currentValue.length(), lhs.length());
          int diff = replacementValue.length() - currentValue.length();

          for(ValuePair vp : values) {
            if(vp.index > index) { vp.index += diff; }
          }
          
          index = lhs.indexOf(currentValue);
        }
      }

      for(ValuePair kv : values) {
        lhs = lhs.substring(0, kv.index) + kv.value +
          lhs.substring(kv.index + kv.value.length(), lhs.length());
      }

      System.out.println(lhs);
    }
  }
}

File.open(ARGV[0]).each_line do |line|
  next if line.empty?
  puts line.split(" ").map {|word| word[0].upcase + word[1..-1]}.join(" ")
end

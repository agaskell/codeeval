package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strings"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {

    s := scanner.Text()

    previousWasSpace := false
    for i := range s {
      c := string(s[i])
      if i == 0 || previousWasSpace {
        c = strings.ToUpper(string(s[i]))
      }

      previousWasSpace = (c == " ")
      fmt.Print(c)
    }

    fmt.Println("")
  }
}

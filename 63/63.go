package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strconv"
import s "strings"
import "math/big"

const yes = 1
const no = 2

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  primes := make(map[int]int)

  for scanner.Scan() {
    num := s.Split(scanner.Text(), ",")

    a, _ := strconv.Atoi(num[0])
    b, _ := strconv.Atoi(num[1])

    count := 0
    for i := a; i <= b; i++ {
      if primes[i] == 0 {
        b := big.NewInt(int64(i))
        isPrime := b.ProbablyPrime(0)
        if isPrime {
          primes[i] = yes
        } else {
          primes[i] = no
        }
      }
      if primes[i] == yes {
        count++
      }
    }

    fmt.Println(count);
  }
}

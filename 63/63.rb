require 'prime'

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  n, m = line.split(",").map(&:to_i)

  count = 0
  n.upto(m) { |i| count += 1 if i.prime? }

  puts count
end

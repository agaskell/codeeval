File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  numbers = line.split(" ").map(&:to_i)
  numbers.delete_at(0)

  ary = []
  1.upto(numbers.length - 1) { |i| ary.push(i) }
  0.upto(numbers.length - 2) do |i|
    ary.delete((numbers[i] - numbers[i + 1]).abs)
  end
  puts ary.length === 0 ? "Jolly" : "Not jolly"

end

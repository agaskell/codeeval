package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strconv"
import s "strings"
import "math"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    a := s.Split(scanner.Text(), " ")
    slice := a[1:len(a)]

    numbers, ary := make([]int, len(slice)), make([]int, 0)

    for i := range slice {
      numbers[i], _ = strconv.Atoi(slice[i])
    }

    ln := len(numbers)
    for i := 1; i < ln; i++ { ary = append(ary, i) }

    ln -= 1
    for i := ln; i < ln; i++ {
      r := int(math.Abs(float64(numbers[i] - numbers[i + 1])))

      for j := 0; j < len(numbers); j++ {
        if numbers[j] == r {
          copy(numbers[j:], numbers[j + 1:])
          numbers[len(numbers) - 1] = 0
          numbers = numbers[:len(numbers)-1]
        }
      }
    }

    message := "Not jolly"
    if len(numbers) == 0 { message = "Jolly"}
    fmt.Println(message);
  }
}

/*
File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  numbers = line.split(" ").map(&:to_i)
  numbers.delete_at(0)

  ary = []
  1.upto(numbers.length - 1) { |i| ary.push(i) }
  0.upto(numbers.length - 2) do |i|
    ary.delete((numbers[i] - numbers[i + 1]).abs)
  end
  puts ary.length === 0 ? "Jolly" : "Not jolly"

end
*/

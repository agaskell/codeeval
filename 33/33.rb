list = []

0.upto(44000) { |i| list[i] = i * i }

def binary_search(array, value, from=0, to=nil)
    if to == nil
        to = array.count - 1
    end

    mid = (from + to) / 2

    if from > to
      return mid
    end

    if value < array[mid]
        return binary_search array, value, from, mid - 1
    elsif value > array[mid]
        return binary_search array, value, mid + 1, to
    else
        return mid
    end
end

def binary_contains(array, value, from=0, to=nil)
    if to == nil
        to = array.count - 1
    end

    mid = (from + to) / 2

    if from > to
      return false
    end

    if value < array[mid]
        return binary_contains array, value, from, mid - 1
    elsif value > array[mid]
        return binary_contains array, value, mid + 1, to
    else
        return true
    end
end


n = 0
File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  if n == 0
    n = line.to_i
    next
  end

  c = line.to_i

  ary = []

  r = binary_search list, c
  candidates = list[0..r]

  l = candidates.length / 2

  (candidates.length - 1).downto(l) do |i|
    if binary_contains(candidates, c - candidates[i])
      ary << candidates[i] unless ary.include? candidates[i]
      ary << c - candidates[i] unless ary.include? c - candidates[i]
    end
  end

  puts ary.length / 2
end

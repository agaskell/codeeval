list = []

0.upto(1000) { |i| list[i] = i * i }

n = 0
File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  if n == 0
    n = line.to_i
    next
  end

  c = line.to_i

  count = 0
  candidates = list.select { |i| i <= c}

  (candidates.length - 1).downto(candidates.length * 3 / 4) do |i|
    #puts "#{c} - #{candidates[i]} = #{c - candidates[i]}"

    if candidates.include?(c - candidates[i])
      count += 1
    end
  end

  puts count
end

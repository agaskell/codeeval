n = 0
File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  if n == 0
    n = line.to_i
    next
  end

  i = line.to_i

  o = "0"
  o = "1" if i % 4 != 3

  if i > 1
    sq = Math.sqrt(i).to_i

    if sq * sq == i || (sq + 1) * (sq + 1) == i
      o = "2"
    end
  end

  puts o
end

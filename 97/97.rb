File.open(ARGV[0]).each_line do |line|
  next if line.strip.empty?
  keys, indexes = line.split("|")
  indexes = indexes.split(" ").map(&:to_i)

  indexes.each { |i| putc keys[i - 1] }
  puts ""
end

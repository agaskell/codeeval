package main

import "fmt"
import "log"
import "bufio"
import "os"
import "regexp"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    line := scanner.Text()
    m, _ := regexp.MatchString("^((\\\"([a-zA-Z0-9\\-_.+@])+\\\")|([a-zA-Z0-9\\-_.+])+)@+([a-zA-Z0-9\\-_.+])+$", line)

    fmt.Println(m)
  }
}

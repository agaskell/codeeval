File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  s = line

  ary = [s[0]]
  length = s.length
  1.upto(s.length - 1) do |i|
    unless ary.index(s[i]).nil?
      length = i
      break
    end
  end
  puts length
end

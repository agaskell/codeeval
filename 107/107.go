package main

import "fmt"
import "log"
import "bufio"
import "os"
//import s "strings"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  for scanner.Scan() {
    line := scanner.Text()

    l := len(line)
    length := l

    for i := 1; i < l; i++ {
      if line[0] == line[i] {
        length = i;
        break;
      }
    }

    fmt.Println(length)
  }
}

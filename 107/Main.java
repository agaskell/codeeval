import java.io.*;

public class Main  {
  public static void main (String[] args) throws FileNotFoundException, IOException {
    File file = new File(args[0]);
    BufferedReader in = new BufferedReader(new FileReader(file));
    String line;
    while ((line = in.readLine()) != null) {
      int l = line.length();
      int length = l;

      for(int i = 1; i < l; i++) {
        if(line.charAt(i) == line.charAt(0)) {
          length = i;
          break;
        }
      }

      System.out.println(length);
    }
  }
}

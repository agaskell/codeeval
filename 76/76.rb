File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?
  a, b = line.split(",").map { |m| m.split("") }

  match = false
  0.upto(b.length - 1) do |i|
    a.push(a.shift)
    if a == b
      match = true
      break
    end
  end

  puts match ? "True" : "False"
end

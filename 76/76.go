package main

import "fmt"
import "log"
import "bufio"
import "os"
import s "strings"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    words := s.Split(scanner.Text(), ",")
    a := words[0]
    b := words[1]

    match := false
    for i := range b {
      if a == (b[len(b) - i:len(b) ] + b[0:len(b) - i]) {
        match = true
      }
    }
    o := "False"
    if match {
      o = "True"
    }
    fmt.Println(o)
  }
}

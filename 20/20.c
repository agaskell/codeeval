#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main(int argc, char *argv[]) {
  FILE *f = fopen(argv[1], "r");
  char line[1024];

  while (fgets(line, 1024, f)) {
    if (line[0] == '\n') {
        continue;
    }

    for(int i = 0; line[i]; i++) {
      printf("%c", tolower(line[i]));
    }
  }

  return 0;
}

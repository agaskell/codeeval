package main

import "fmt"
import "log"
import "bufio"
import "os"
import s "strings"
import "strconv"

/*
def process_array ary, r = 0, c = 0, total = 0
  return nil if ary.nil? || ary.empty? || ary[r].nil? || ary[r][c].nil?

  total += ary[r][c]
  if r == ary.length - 1 && c == ary.length - 1
    #puts "r = #{r}, c = #{c}, total = #{total}"
    @totals << total
  else
    process_array ary, r + 1, c, total
    process_array ary, r, c + 1, total
  end
end */

func ProcessArray(ary [][]int, r int, c int, size int, total int, totals *[]int) {
  if r == size || c == size {
    return
  }

  total += ary[r][c]
  if r == size - 1 && c == size - 1 {
    *totals = append(*totals, total)
  } else {
    ProcessArray(ary, r + 1, c, size, total, totals)
    ProcessArray(ary, r, c + 1, size, total, totals)
  }
}

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  ary := make([][]int, 0)

  size := 0
  i := 0

  for scanner.Scan() {
    line := scanner.Text()

    if s.Index(line, ",") == -1 {
      size, _ = strconv.Atoi(line)
      ary = make([][]int, size)
      i = 0
    } else {
      tempArray := s.Split(line, ",")
      numArray := make([]int, len(tempArray))

      for j := range tempArray {
        numArray[j], _ = strconv.Atoi(tempArray[j])
      }

      ary[i] = numArray

      i++
      if(i == size) {
        totals := make([]int, 0)
        ProcessArray(ary, 0, 0, size, 0, &totals)

        min := totals[0]
        for j := range totals {
          if totals[j] < min {
            min = totals[j]
          }
        }

        fmt.Println(min)
      }
    }
  }
}

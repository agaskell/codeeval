ary =[]

@totals = []

def process_array ary, r = 0, c = 0, total = 0
  return nil if ary.nil? || ary.empty? || ary[r].nil? || ary[r][c].nil?

  total += ary[r][c]
  if r == ary.length - 1 && c == ary.length - 1
    #puts "r = #{r}, c = #{c}, total = #{total}"
    @totals << total
  else
    process_array ary, r + 1, c, total
    process_array ary, r, c + 1, total
  end
end

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  if line.index(",").nil?
    @totals = []
    process_array ary
    ary = []
    puts @totals.min unless @totals.empty?
  else
    ary << line.split(",").map(&:to_i)
  end
end

@totals = []
process_array ary
puts @totals.min

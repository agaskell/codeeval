def build_keys key
  key = key.split ""

  h = Hash.new
  length = 1
  i, limit = 0, 2 ** length - 1
  until key.empty?
    c = key.shift
    h[i.to_s(2).rjust(length, "0")] = c
    i += 1

    if i == limit
      length += 1
      limit = 2 ** length - 1
      i = 0
    end
  end
  h
end

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  zero_index = line.index("0")
  one_index = line.index("1")

  delim = [zero_index, one_index].min

  keys = build_keys line[0...delim]
  message = line[delim..-1]

  #puts message[0...3].inspect
  output = ""
  until message[0...3] == "000"
    char_length = message[0...3]
    char_length = char_length.to_i(2)
    message = message[3..-1]

    current_delim = "1" * char_length
    current = ""
    until current == current_delim
      current = message[0...char_length]
      message = message[char_length..-1]
      o = keys[current]
      output += o unless o.nil?
    end

  end

  puts output
end

=begin
0     0 length 1  2 ^ 1 - 1
00    0 length 2  2 ^ 2 - 1
01    1 length 2
10    2 length 2
000   0 length 3  2 ^ 3 - 1
001   1 length 3
010   2 length 3
011   3 length 3
100   4 length 3
101   5 length 3
110   6 length 3
0000  0 length 4  2 ^ 4 - 1
0001  1 length 4
0010  2 length 4
=end

class Brick
  attr_accessor :id

  def fit? hole
    b = []
    b << (@coord1[0] - @coord2[0]).abs
    b << (@coord1[1] - @coord2[1]).abs
    b << (@coord1[2] - @coord2[2]).abs

    b.sort!

    return b.first <= hole.first && b[1] <= hole.last
  end

  def initialize s
    s = s.gsub("(", "").gsub(")", "").split(" ")
    @id = s[0].to_i
    @coord1 = parse_coords(s[1])
    @coord2 = parse_coords(s[2])
  end
end

def parse_coords c
  s = c.index("[") + 1
  e = c.index("]")
  c[s...e].split(",").map(&:to_i)
end

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  hole, brick_strings = line.split("|")
  hole = hole.split(" ").map {|m| parse_coords(m) }.sort
  w = (hole[0][0] - hole[1][0]).abs
  h = (hole[0][1] - hole[1][1]).abs
  hole = [w, h].sort

  bricks = []
  brick_strings.split(";").each { |b| bricks << Brick.new(b)}

  fit = []
  bricks.each { |b| fit << b.id if b.fit? hole }

  puts fit.empty? ? "-" : fit.sort.join(",")
end

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  s = line
  l = s.length

  total = 0
  0.upto(l - 1) do |i|
    total += line[i].to_i ** l
  end

  puts (total === s.to_i) ? "True" : "False"
end

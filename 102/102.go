package main

import "fmt"
import "log"
import "bufio"
import "os"
import s "strings"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  for scanner.Scan() {
    line := s.Split(scanner.Text(), " ")

    longest := ""
    length := len("")

    for i := range line {
      if len(line[i]) > length {
        longest = line[i]
        length = len(longest)
      }
    }

    fmt.Println(longest)
  }
}

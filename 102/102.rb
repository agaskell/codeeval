require 'rubygems'
require 'json'

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  a = JSON.parse(line)
  #puts a["menu"]["items"].inspect

  total = 0
  a["menu"]["items"].each do |i|
    total += i["id"] unless i.nil? || i["label"].nil?
  end
  puts total
end

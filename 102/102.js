var fs  = require("fs");
fs.readFileSync(process.argv[2]).toString().split('\n').forEach(function (line) {
    if (line != "") {
      var a = JSON.parse(line);

      if(!a || !a.menu || !a.menu.items) {
        console.log(0);
      } else {
        var sum = 0;
        for(var i = 0; i < a.menu.items.length; i++) {
          if(a.menu.items[i] && a.menu.items[i].label) {
            sum += a.menu.items[i].id;
          }
        }
        console.log(sum);
      }

    }
});

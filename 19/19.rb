File.open(ARGV[0]).each_line do |line|
  next if line.empty?
  number, p1, p2 = line.split(',').map(&:to_i)

  bin = number.to_s(2)
  puts bin[-p1] == bin[-p2]
end

package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strings"
import "strconv"

func main() {
    file, err := os.Open(os.Args[1])
    if err != nil {
        log.Fatal(err)
    }
    defer file.Close()
    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        s := strings.Split(scanner.Text(), ",")
        num, _ := strconv.Atoi(s[0])
        a, _ := strconv.Atoi(s[1])
        b, _ := strconv.Atoi(s[2])
        bin := strconv.FormatInt(int64(num), 2)
        l := len(bin)
        fmt.Println(bin[l - a] == bin[l - b])
    }
}

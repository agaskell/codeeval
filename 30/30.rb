File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  ary1, ary2 = line.split(";")
  ary1 = ary1.split(",")
  ary2 = ary2.split(",")


  puts (ary1 & ary2).join(",")
end

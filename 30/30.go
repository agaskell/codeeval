package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strings"
//import "strconv"
import "sort"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    ary := strings.Split(scanner.Text(), ";")
    nums1 := strings.Split(ary[0], ",")
    nums2 := strings.Split(ary[1], ",")

    m := make(map[string]int);

    for i := range nums1 {
      m[nums1[i]] += 1
    }

    for i := range nums2   {
      m[nums2[i]] += 1
    }

    out := make([]string, 0)

    for k, v := range m {
      if v > 1 {
        out = append(out, k)
      }
    }

    sort.Strings(out)

    fmt.Println(strings.Join(out, ","))
  }
}

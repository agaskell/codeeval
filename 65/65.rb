@board = ["ABCE", "SFCS", "ADEE"]

@board.each_with_index do |b, i|
  @board[i] = b.split ""
end

def find_linked_chars seg, positions
  char_to_find = seg[0]
  last_pos = positions.last

  ary = []
  @board.each_with_index do |row, i|
    row.each_with_index do |cell, j|
      ary << [cell, [i, j]] if cell == char_to_find && !positions.include?([i, j])
    end
  end

  ary.each do |a|
    if (((last_pos[0] - a[1][0]).abs == 1 && last_pos[1] == a[1][1]) ||
      ((last_pos[1] - a[1][1]).abs == 1) && last_pos[0] == a[1][0])

      return true if seg.length == 1
      r = find_linked_chars seg[1..-1], positions + [a[1]]
      return r if r
    end
  end

  return false
end

def find_word w
  char_to_find = w[0]
  @board.each_with_index do |row, i|
    row.each_with_index do |col, j|
      if col == char_to_find
        ary = []
        ary << [i,j]
        res = find_linked_chars w[1..-1], ary
        return true if res
      end
    end
  end

  return nil
end

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  ary, pos, r = [], nil, false
  r = find_word line

  puts r ? "True" : "False"
end

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  n, m = line.split(",").map(&:to_i)

  a = n / m
  puts n - a * m
end

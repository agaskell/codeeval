#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
  FILE *f = fopen(argv[1], "r");
  char line[1024];

  while (fgets(line, 1024, f)) {
    if (line[0] == '\n') {
        continue;
    }

    char *tok = strtok(&line[0], ",");

    int n = atoi(tok);
    tok = strtok(NULL, " ");
    int m = atoi(tok);

    printf("%d\n", n % m);
  }

  return 0;
}

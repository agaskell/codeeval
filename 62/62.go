package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strings"
import "strconv"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    s := strings.Split(scanner.Text(), ",")

    n, _ := strconv.Atoi(s[0])
    m, _ := strconv.Atoi(s[1])

    a := n / m
    fmt.Println(n - a * m)
  }
}

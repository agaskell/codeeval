nums = []

File.open(ARGV[0]).each_line do |line|
  next if line.empty?
  line = line.split(' ').map(&:to_i)

  if line.length == 1
    min = nil
    until nums.empty?
      n = nums.shift

      nums.each do |current|
        t = (current.first - n.first) ** 2 + (current.last - n.last) ** 2
        min = t if min.nil?
        min = t if t < min
      end
    end

    min = Math.sqrt(min) unless min.nil?
    puts min >= 10000 ? "INFINITY" : "#{format("%.4f", min)}" unless min.nil?
    nums = []
  end
  nums << line unless line.length == 1
end

package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strings"
import "strconv"
import "math"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  num := make([][2]int, 0)
  count, min := 0, 0

  for scanner.Scan() {
    s := strings.Split(scanner.Text(), " ")

    if len(s) == 1 {
      c, _ := strconv.Atoi(s[0])

      if(count > 0) {
        out := math.Sqrt(float64(min))
        if(out >= 10000) {
          fmt.Println("INFINITY")
        } else {
          fmt.Printf("%.4f\n", out)
        }
      }

      min, count = 0, 0
      num = make([][2]int, c)
    } else {
      a, _ := strconv.Atoi(s[0])
      b, _ := strconv.Atoi(s[1])

      for i := count - 1; i >= 0; i-- {
        x1 := a - num[i][0]
        y1 := b - num[i][1]

        v := (x1 * x1 + y1 * y1)
        if(v < min || min == 0) {
          min = v
        }
      }

      num[count] = [2]int {a, b}
      count++
    }
  }
}

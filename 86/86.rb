@map {
  "2" => 2,
  "3" => 3,
  "4" => 4,
  "5" => 5,
  "6" => 6,
  "7" => 7,
  "8" => 8,
  "9" => 9,
  "T" => 10,
  "J" => 11,
  "Q" => 12,
  "K" => 13,
  "A" => 14,
}

def to_values h
  h.sort { |a, b| @map[a[0]] <=> @map[b[0]]}.map { |m| @map[m[0]] }
end


def same_suit? h
  suit = h[0][1]
  h.find { |c| c[1] != suit}.nil?
end

def of_a_kind? h, n
  temp = Hash.new(0)
  h.each { |c| temp[c.first] += 1 }

  temp.each do |set|
    if set.last.to_i == n
      h.delete_if { |i| i.first = set.first }
      return h
    end
  end
  h
end

def consecutive? h
  #an array of card values
  s = to_values h
  first = s.first
  1.upto(s.length - 1) do |i|
    return false if s.first != s[i] + i
  end
  true
end

def royal_flush h
  return false unless same_suit? h
  s = h.sort.map { |m| m[0] }
  if s == ["A", "J", "K", "Q", "T"]
    return []
  end
  return h
end

def straight_flush? h
  if same_suit? h && consecutive? h
    return []
  end
  return h
end

def four_of_a_kind? h
  return of_a_kind? h, 4
end

def full_house? h
  result = of_a_kind? 3

  if result.length == 2
    result = of_a_kind? 2
    return result.empty? ? [] : h
  end
  return h
end

def flush? h
  same_suit? h
end

def straight? h
  consecutive? h
end

def three_of_a_kind? h
  return of_a_kind? h, 3
end

def pair? h
  return of_a_kind? h, 2
end

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  line = line.split(" ")
  a, b = line[0..4], line[5..-1]



end

package main

import "fmt"
import "log"
import "bufio"
import "os"
import s "strings"
import "strconv"
import "math"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  for scanner.Scan() {
    line := s.Split(scanner.Text(), " ")
    count := 0

    length, _ := strconv.Atoi(line[0])
    length -= 6

    distance, _ := strconv.Atoi(line[1])

    bs := line[3:]

    bats := make([]int, len(line[3:]))

    for i := range bs {
      bats[i], _ = strconv.Atoi(bs[i])
    }

    i := 6
    for i <= length {
      for j := range bats {
        if int(math.Abs(float64(i - bats[j]))) < distance {
          i = bats[j] + distance
        }
      }

      if i <= length { count++ }
      i += distance
    }

    fmt.Println(count)
  }
}

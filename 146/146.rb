File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  line = line.split(" ").map(&:to_i)

  length = line[0] - 6
  distance = line[1]
  number = line[2]

  bats = line[3..-1]

  count = 0
  i = 6
  while i <= length
    bats.each do |bat|
      if (i - bat).abs < distance
        i = bat + distance
      end
    end

    count += 1 if i <= length
    i += distance
  end

  puts count
end

def add_letter letters, ary, length
  i = letters.index(ary.pop)

  while letters[i] == letters.last
    i = letters.index(ary.pop)
    return false if i.nil? && ary.empty?
  end

  ary.push(letters[i + 1])

  until ary.length == length
    ary.push(letters.first)
  end

  true
end

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  n, letters = line.split(",")
  n = n.to_i

  letters = letters.split("").uniq.sort

  o = (letters.first * n).split ""

  out = o.join

  while add_letter letters, o, n
    out += "," + o.join
  end

  puts out
end

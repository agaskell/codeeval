package main

import "fmt"
import "log"
import "bufio"
import "os"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  m := map[string]string {
    "(" : ")",
    "{" : "}",
    "[" : "]",
  }

  for scanner.Scan() {
    line := scanner.Text()

    l := len(line)
    if(l % 2 == 1) {
      fmt.Println("False")
      continue
    }

    stack := make([]string, 0)
    result := "True"

    for i := 0; i < l; i++ {
      c := string(line[i])

      if c == "(" || c == "[" || c == "{" {
        stack = append(stack, c)
      } else {
        if(len(stack) == 0) {
          result = "False"
          break
        }
        p := stack[len(stack) - 1]
        stack = stack[:len(stack) - 1]
        if m[p] != c {
          result = "False"
          break
        }
      }
    }

    fmt.Println(result)
  }
}

@map = {
  "(" => ")",
  "{" => "}",
  "[" => "]",
}

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  l = line.length
  if l % 2 == 1
    puts "False"
    next
  end

  stack = []
  result = "True"

  l -= 1
  0.upto(l) do |i|
    char = line[i]

    if @map.keys.include? char
      stack.push(line[i])
    else
      p = @map[stack.pop]
      unless p == char
        result = "False"
        break
      end
    end
  end

  puts result
end

def parse_coords c
  s = c.index("(") + 1
  e = c.index(")")
  c[s...e].split(",").map(&:to_i)
end

def distance p1, p2
  return Math.sqrt((p1.first - p2.first) ** 2 + (p1.last - p2.last) ** 2)
end

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?
  points = line.split(", ").map { |p| parse_coords p }

  d = Hash.new(0)
  points.each do |p|
    points.each do |p1|
      d[distance(p, p1)] += 1 unless p == p1
    end
  end

  puts d.count == 2 && d.values.sort == [4, 8]
end

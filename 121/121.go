package main

import "fmt"
import "log"
import "bufio"
import "os"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  m := map[string]string {
    " " : " ",
    "a" : "y",
    "b" : "h",
    "c" : "e",
    "d" : "s",
    "e" : "o",
    "f" : "c",
    "g" : "v",
    "h" : "x",
    "i" : "d",
    "j" : "u",
    "k" : "i",
    "l" : "g",
    "m" : "l",
    "n" : "b",
    "o" : "k",
    "p" : "r",
    "q" : "z",
    "r" : "t",
    "s" : "n",
    "t" : "w",
    "u" : "j",
    "v" : "p",
    "w" : "f",
    "x" : "m",
    "y" : "a",
    "z" : "q",
  }

  for scanner.Scan() {
    line := scanner.Text()
    s := ""

    for _, i := range line {
      s += m[string(i)]
    }
    fmt.Println(s)
  }
}

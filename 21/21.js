var fs  = require("fs");

fs.readFileSync(process.argv[2]).toString().split('\n').forEach(function (line) {
  if (line != "") {
    var nums = line.split("");

    var total = 0;
    for(var i = 0; i < nums.length; i++) {
      total += parseInt(nums[i]);
    }

    console.log(total);
  }
});

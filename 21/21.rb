File.open(ARGV[0]).each_line do |line|
  next if line.empty?
  total = 0
  (0...line.length).each { |i| total += line[i].to_i }
  puts total
end

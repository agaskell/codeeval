using System.IO;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        using (StreamReader reader = File.OpenText(args[0]))
        while (!reader.EndOfStream)
        {
            string line = reader.ReadLine();
            if (null == line)
                continue;

            var total = 0;
            foreach(var c in line)
            {
                total += int.Parse(c.ToString());
            }
            System.Console.WriteLine(total);
        }
    }
}

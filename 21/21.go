package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strconv"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    s := scanner.Text()

    sum := 0
    for _, c := range s {
      i, _ := strconv.Atoi(string(c))
      sum += i
    }

    fmt.Println(sum)
  }
}

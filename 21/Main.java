import java.io.*;

public class Main  {
  public static void main (String[] args) throws FileNotFoundException, IOException {
    File file = new File(args[0]);
    BufferedReader in = new BufferedReader(new FileReader(file));
    String line;
    while ((line = in.readLine()) != null) {
      String[] nums = line.trim().split("");

      int total = 0;
      for(String s : nums) {
        if(s.length() > 0) {
          total += Integer.parseInt(s);
        }
      }

      System.out.println(total);
    }
  }
}

var fs  = require("fs");

fs.readFileSync(process.argv[2]).toString().split('\n').forEach(function (line) {
    if (line != "") {
      var i = parseInt(line);

      var fib = [0, 1];

      while(fib.length < i + 1) {
        fib.push(fib[fib.length - 1] + fib[fib.length - 2]);
      }

      console.log(fib[i]);
    }
});

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
  FILE *f = fopen(argv[1], "r");
  char line[1024];

  while (fgets(line, 1024, f)) {
    if (line[0] == '\n') {
        continue;
    }

    int index = atoi(line);

    if(index == 0 || index == 1) {
      printf("%d\n", index);
      continue;
    }

    int fib[index + 1];
    fib[0] = 0;
    fib[1] = 1;

    for(int i = 2; i <= index; i++) {
      fib[i] = fib[i - 1] + fib[i - 2];
    }

    printf("%d\n", fib[index]);
  }

  return 0;
}

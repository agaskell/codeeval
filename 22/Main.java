import java.io.*;

public class Main  {
  public static void main (String[] args) throws FileNotFoundException, IOException {
    File file = new File(args[0]);
    BufferedReader in = new BufferedReader(new FileReader(file));
    String line;
    while ((line = in.readLine()) != null) {
      int index = Integer.parseInt(line);

      if(index == 0 || index == 1) {
        System.out.println(index);
        continue;
      }

      int[] fib = new int[index + 1];
      fib[0] = 0;
      fib[1] = 1;

      for(int i = 2; i <= index; i++) {
        fib[i] = fib[i - 1] + fib[i - 2];
      }

      System.out.println(fib[index ]);
    }
  }
}

package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strconv"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  fib := make([]int, 0)
  fib = append(fib, 0)
  fib = append(fib, 1)

  for scanner.Scan() {
    i, _ := strconv.Atoi(scanner.Text())

    for len(fib) <= i {
      fib = append(fib, fib[len(fib) - 2] + fib[len(fib) - 1])
    }
    fmt.Println(fib[i])
  }
}

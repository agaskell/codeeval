package main

import "fmt"
import "log"
import "bufio"
import "os"
import s "strings"
import "strconv"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  for scanner.Scan() {
    line := s.Split(scanner.Text(), ";")

    i, _ := strconv.Atoi(line[1])
    nums := s.Split(line[0], ",")

    o := make([]string, 0)

    for i <= len(nums) {
      for j := i - 1; j >= 0; j-- {
        o = append(o, nums[j])
      }
      nums = nums[i:]
    }

    output := s.Join(o, ",")

    if len(o) != 0 && len(nums) != 0 {
      output += ","
    }

    output += s.Join(nums, ",")
    fmt.Println(output)
  }
}

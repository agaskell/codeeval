File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  line, i = line.split(";")
  i = i.to_i

  nums = line.split(",")

  out = []
  while i <= nums.length
    t = nums[0..(i - 1)].reverse
    out << t
    nums = nums - t
  end

  s = out.join(",")
  s += "," unless nums.empty? || out.empty?
  s += nums.join(",") unless nums.empty?
  puts s
end

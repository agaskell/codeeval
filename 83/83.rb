File.open(ARGV[0]).each_line do |line|
  next if line.empty?

  line = line.downcase.gsub(/\p{^Alpha}/, '')

  chars = Hash.new(0)

  0.upto(line.length - 1) { |i| chars[line[i]] += 1 }

  chars = chars.sort_by {|k,v| v}.reverse

  total = 0
  current_value = 26

  chars.each do |char_hash|
    total += current_value * char_hash[1]
    current_value -= 1
  end

  puts total
end

require 'prime'

def palindrome?(n)
  s = n.to_s
  mid = s.length / 2

  palin = true
  for i in 0..mid
    palin = false if s[i] != s[i * -1 -1]
    break if !palin
  end
  palin
end

1000.downto(2).each do |i|
  if Prime.prime?(i) && palindrome?(i)
    puts i
    break
  end
end

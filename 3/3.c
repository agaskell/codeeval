#include <stdio.h>
#include <string.h>
#include <limits.h>

#define ENOUGH ((CHAR_BIT * sizeof(int) - 1) / 3 + 2)

int isPalindrome(int n) {
  char str[ENOUGH];
  sprintf(str, "%d", n);
  size_t len = strlen(str);

  int palin = 1;
  int mid = len / 2;

  for(int i = 0; i <= mid; i++) {
    if(str[i] != str[len - i - 1]) {
      palin = 0;
    }
  }

  return palin;
}

int isPrime(int n) {
  for(int i = 2; i * i <= n; i++) {
    if (n % i == 0) { return 0; }
  }
  return 1;
}

int main() {
  for(int i = 997; i >= 0; i--) {
    if(isPalindrome(i) && isPrime(i)) {
      printf("%d\n", i);
      break;
    }
  }
  return 0;
}

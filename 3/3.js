function isPrime(val) {
  for(var i = 2; i * i <= val; i++) {
    if (val % i == 0) { return false; }
  }
  return true;
}

function isPalindrome(val) {
  var s = val.toString();
  var mid = Math.floor(s.length / 2);

  var palin = true;
  for(k = 0; k <= mid; k++) {
    if(s[k] !== s[s.length - k - 1]) {
      palin = false;
      break;
    }
  }
  return palin;
}

for(var j = 997; j >= 0; j--) {
    if(isPalindrome(j) && isPrime(j)) {
    console.log(j);
    break;
  }
}

package main

import "fmt"
import "math/big"
import "strconv"

func IsPalindrome(i int) bool {
  s := strconv.Itoa(i)
  l := len(s)
  mid := l / 2
  l -= 1

  palin := true
  for i := 0; i <= mid; i++ {
    if s[i] != s[l - i] {
      palin = false
      break
    }
  }

  return palin;
}

func main() {
  for i := 997; i >= 0; i -= 2 {
    b := big.NewInt(int64(i))
    if b.ProbablyPrime(0) && IsPalindrome(i) {
      fmt.Println(i)
      break
    }
  }
}

public class Main  {
  private static boolean isPrime(int n) {
    for(int i = 2; i * i <= n; i++) {
      if (n % i == 0) { return false; }
    }
    return true;
  }

  private static boolean isPalindrome(int n) {
    String s = n + "";
    int mid = s.length() / 2;

    boolean palin = true;
    for(int i = 0; i <= mid; i++) {
      if(s.charAt(i) != s.charAt(s.length() - i - 1)) {
        palin = false;
        break;
      }
    }
    return palin;
  }

  public static void main (String[] args) {
    for(int i = 997; i >= 0; i--) {
      if(isPalindrome(i) && isPrime(i)) {
        System.out.println(i);
        break;
      }
    }
  }
}

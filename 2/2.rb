lines = []
number = nil

File.open(ARGV[0]).each_line do |line|
  unless number
    number = line.to_i
    next
  end

  lines.push(line)
end

puts lines.sort_by { |l| l.length }.reverse.take(number)

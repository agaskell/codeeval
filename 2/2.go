package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strconv"
import "sort"

type StringLengths []string

func (s StringLengths) Len() int {
  return len(s)
}

func (s StringLengths) Swap(i, j int) {
  s[i], s[j] = s[j], s[i]
}

func (s StringLengths) Less(i, j int) bool {
  return len(s[i]) > len(s[j])
}

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  num := -1

  m := make([]string, 0)

  for scanner.Scan() {
    if num < 0 {
      num, _ = strconv.Atoi(scanner.Text())
      continue
    }

    m = append(m, scanner.Text())
  }

  sort.Sort(StringLengths(m))

  for i := 0; i < num; i++ {
    fmt.Println(m[i])
  }
}

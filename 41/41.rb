File.open(ARGV[0]).each_line do |line|
  next if line.strip.empty?

  garbage, values = line.strip.split(";")
  values = values.split(",")

  puts Hash[values.group_by {|x| x}
    .map { |k, v| [k, v.count]}]
    .sort_by {|k, v| v}.last[0] 
end

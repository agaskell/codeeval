require 'date'

def get_days ary, i
  current, current_end_date = ary[i], ary[i].last
  count = (current[1] - current[0]).to_i

  gap = 0

  0.upto(i - 1) do |j|
    if current.first <= ary[j].last
      if current.last < ary[j].last
        return 0
      else
        if gap >= current.first - ary[j].last
          gap = (current.first - ary[j].last).to_i - 1
        end
      end
    end
  end

  return count + gap
end

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  date_ranges = line.split(";").map { |d| d.split("-").map(&:strip) }.each do |a|
    a[0] = Date.parse(a[0])
    a[1] = (Date.parse(a[1]) >> 1) - 1
  end

  date_ranges.sort! { |a, b| [a.first, (a.first - a.last).to_i] <=> [b.first, (b.first - b.last).to_i] }

  total = 0

  0.upto(date_ranges.length - 1) do |i|
    total += get_days(date_ranges, i)
  end

  puts (total / 365).to_i
end

@map = {
  "negative" => "-",
  "zero" => 0,
  "one" => 1,
  "two" => 2,
  "three" => 3,
  "four" => 4,
  "five" => 5,
  "six" => 6,
  "seven" => 7,
  "eight" => 8,
  "nine" => 9,
  "ten" => 10,
  "eleven" => 11,
  "twelve" => 12,
  "thirteen" => 13,
  "fourteen" => 14,
  "fifteen" => 15,
  "sixteen" => 16,
  "seventeen" => 17,
  "eighteen" => 18,
  "nineteen" => 19,
  "twenty" => 20,
  "thirty" => 30,
  "forty" => 40,
  "fifty" => 50,
  "sixty" => 60,
  "seventy" => 70,
  "eighty" => 80,
  "ninety" => 90
}

@tens_map = {
  "hundred" => 100,
  "thousand" => 1000,
  "million" => 1000000
}

def do_segment num, str
  total = 0
  if num.include? str
    i = num.index str

    seg = num[0...num.index(str)]

    if seg.include? "hundred"
      index = seg.index("hundred")
      seg[0...index].each { |n| total += @map[n] }
      total *= 100
      seg = seg[index + 1..-1]
    end

    seg.each { |n| total += @map[n]}
    total *= @tens_map[str]
    0.upto(num.index(str)).each { |i| num.shift }
  end
  total
end

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  num = line.split(" ")

  neg = false
  if num[0] == "negative"
    num.shift
    neg = true
  end

  mill = do_segment(num, "million")
  k = do_segment(num, "thousand")
  hundred = do_segment(num, "hundred")

  ones = 0
  until num.empty?
    a = num.shift
    ones += @map[a]
  end

  sum = mill + k + hundred + ones
  sum = -sum if neg
  puts sum
end

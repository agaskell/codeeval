@m = {
  10000 => 'ONE HUNDRED',
  5000 => 'FIFTY',
  2000 => 'TWENTY',
  1000 => 'TEN',
  500 => 'FIVE',
  200 => 'TWO',
  100 => 'ONE',
  50 => 'HALF DOLLAR',
  25 => 'QUARTER',
  10 => 'DIME',
  5 => 'NICKEL',
  1 => 'PENNY'}

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  ary = line.split(';')

  pp, ch =  ary.map { |i| i = (Float(i) * 100).to_i }

  if ch < pp
    puts "ERROR"
    next
  end

  if ch == pp
    puts "ZERO"
    next
  end

  change = ch - pp
  ary = []
  while change != 0
    v = @m.find { |k, v| k <= change }
    while v[0] <= change
      ary << v[1]
      change -= v[0]
    end
  end

  puts ary.join(",")

end

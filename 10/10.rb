File.open(ARGV[0]).each_line do |line|
  next if line.empty?
  line = line.split

  number = line.last.to_i
  next if line.length - (number - 1)  < 0
  puts line[-number - 1]
end

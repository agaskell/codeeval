package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strings"
import "strconv"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    s := strings.Split(scanner.Text(), " ")

    l := len(s)
    n, _ := strconv.Atoi(s[l - 1])

    i := l - n - 1
    if i >= 0 {
      fmt.Println(s[i])
    }
  }
}

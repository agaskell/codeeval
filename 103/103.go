package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strings"
import "strconv"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    ary := strings.Split(scanner.Text(), " ")
    nums := make([]int, len(ary))

    for i := range ary {
      nums[i], _ = strconv.Atoi(ary[i])
    }

    m := make(map[int]int);

    for i := range nums {
      m[nums[i]] += 1
    }

    lowest := 0
    for k, v := range m {
      if v == 1 && (k < lowest || lowest == 0) {
        lowest = k
      }
    }

    if(lowest == 0) {
      fmt.Println(lowest)
    } else {
      for i := range nums {
        if nums[i] == lowest {
          fmt.Println(i + 1)
          break
        }
      }
    }
  }
}

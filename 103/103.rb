File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?
  line = line.split(" ").map(&:to_i)

  v = Hash[line.group_by {|x| x}
    .map { |k, v| [k, v.count]}.sort_by {|k| [k[1], k[0]]}]

  if(v.first[1] == 1)
    puts line.index(v.first[0]) + 1
  else
    puts "0"
  end
end

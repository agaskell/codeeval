class String
  def is_integer?
    self.to_i.to_s == self
  end
end

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?
  ary = line.split(",")

  numbers = []
  words = []

  ary.each do |e|
    if e.is_integer?
      numbers.push(e)
    else
      words.push(e)
    end
  end

  print words.join(",") unless words.empty?
  print "|" unless words.empty? || numbers.empty?
  print numbers.join(",") unless numbers.empty?
  puts
end

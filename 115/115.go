package main

import "fmt"
import "log"
import "bufio"
import "os"
//import "strings"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  lookup := map[rune]string {
    'a' : "0",
    'b' : "1",
    'c' : "2",
    'd' : "3",
    'e' : "4",
    'f' : "5",
    'g' : "6",
    'h' : "7",
    'i' : "8",
    'j' : "9",
  }

  zero, nine, a, j := '0', '9', 'a', 'j'

  for scanner.Scan() {
    s := scanner.Text()

    out := ""
    for _, k := range s {
      if k >= a && k <= j {
        out += lookup[k]
      } else if k >= zero && k <= nine {
        out += string(k)
      }
    }

    if len(out) == 0 {
      out = "NONE"
    }

    fmt.Println(out)
  }
}

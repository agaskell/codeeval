package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strconv"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    num, _ := strconv.Atoi(scanner.Text())

    bs := strconv.FormatInt(int64(num), 2);

    c := 0
    r := rune("1"[0])
    for _, i := range bs {
      if i == r { c++ }
    }

    fmt.Println(c);
  }
}

import java.io.*;

public class Main  {
  public static void main (String[] args) throws FileNotFoundException, IOException {
    File file = new File(args[0]);
    BufferedReader in = new BufferedReader(new FileReader(file));
    String line;
    while ((line = in.readLine()) != null) {
      String str = Integer.toBinaryString(Integer.parseInt(line));

      int count = 0;
      for(int i = 0; i < str.length(); i++) {
        if(str.charAt(i) == '1') { count++; }
      }

      System.out.println(count);
    }
  }
}

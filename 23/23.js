var util = require('util');

for(var i = 1; i <= 12; i++) {
  for(var j = 1; j <= 12; j++) {
    var n = i * j;
    var s = util.format("   %d", n);
    if(n >= 10) { s = util.format("  %d", n); }
    if(n >= 100) { s = util.format(" %d", n); }
    process.stdout.write(s);
  }
  console.log('');
}

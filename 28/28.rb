def has_substring str1, str2
  match = false
  substr_index = 0

  full_index = str1.index(str2[substr_index])

  until full_index.nil? || str1[full_index + substr_index] != str2[substr_index]
    if str2[substr_index + 1] == "\\"
      full_index -= 1
      substr_index += 1
    end
    substr_index += 1
  end

  if str2[substr_index] == "*" && str2[substr_index - 1] != "\\"
    if substr_index = str2.length - 1
      return true
    else
      return false unless has_substring str1[full_index || 0 + substr_index..-1], str2[substr_index + 1..-1]
    end
  else
    if substr_index == str2.length - 1
      return true
    else
      return false if full_index.nil?
      return has_substring str1[(full_index || 0) + substr_index..-1], str2
    end
  end

  true
end

File.open(ARGV[0]).each_line do |line|
  next if line.empty?
  full, su = line.split(',')
  puts has_substring full, su
end

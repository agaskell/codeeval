File.open(ARGV[0]).each_line do |line|
  fib = [0, 1]

  next if line.strip!.empty?

  i = line.to_i + 1

  while(fib.length < i + 1) #we are starting at fib[0]
    fib << fib.last(2).inject(:+)
  end

  puts fib[i]
end

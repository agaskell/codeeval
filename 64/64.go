package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strconv"
import "math/big"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  fib := make([]*big.Int, 0)
  fib = append(fib, big.NewInt(0))
  fib = append(fib, big.NewInt(1))

  for scanner.Scan() {
    i, _ := strconv.Atoi(scanner.Text())
    i++

    for len(fib) <= i {

      a, b := fib[len(fib) - 2], fib[len(fib) - 1]

      t := new(big.Int)

      fib = append(fib, t.Add(a, b))
    }
    fmt.Println(fib[i])
  }
}

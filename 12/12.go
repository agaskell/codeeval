package main

import "fmt"
import "log"
import "bufio"
import "os"
//import s "strings"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  for scanner.Scan() {
    line := scanner.Text()

    h := make(map[rune]int);

    for _, i := range line {
      h[i]++
    }

    for a, b := range h {
      if b == 1 {
        fmt.Println(string(a))
        break
      }
    }
  }
}

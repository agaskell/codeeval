File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  puts line.scan(/\w/).inject(Hash.new(0)){|h, c| h[c] += 1; h}
  .find{|e| e[1] == 1}[0]
end

package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strconv"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    num := scanner.Text()

    match := "0"
    for i := 0; i < 20; i++ {

      sum := 0
      for j := range num {
        dig, _ := strconv.Atoi(string(num[j]))
        sum += dig * dig
      }

      if sum == 1 {
        match = "1"
        break
      }

      num = strconv.Itoa(sum)
    }

    fmt.Println(match)

  }
}

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  i = line.to_i

  happy = false
  0.upto(100) do
    i = i.to_s.split("").map(&:to_i).map! { |e| e ** 2 }.inject(:+)
    if i === 1
      happy = true
      break
    end
  end

  puts happy ? "1" : "0"
end

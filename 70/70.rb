class Rectangle
  attr_accessor :top, :right, :bottom, :left

  def initialize(a)
    @left, @top, @right, @bottom = a[0], a[1], a[2], a[3]
  end

  def overlaps?(r)
    !(@right < r.left || @left > r.right || @top < r.bottom || @bottom > r.top)
  end
end

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  line = line.split(",").map(&:to_i)

  r1 = Rectangle.new(line[0..3])
  r2 = Rectangle.new(line[4..-1])

  puts r1.overlaps?(r2) ? "True" : "False"
end

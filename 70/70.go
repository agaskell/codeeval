package main

import "fmt"
import "log"
import "bufio"
import "os"
import s "strings"
import "strconv"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  for scanner.Scan() {
    line := s.Split(scanner.Text(), ",")

    n := make([]int, len(line))

    for i := 0; i < len(n); i++ {
      n[i], _ = strconv.Atoi(line[i])
    }
    r1_l, r1_t, r1_r, r1_b := n[0], n[1], n[2], n[3]
    r2_l, r2_t, r2_r, r2_b := n[4], n[5], n[6], n[7]

    overlap := !(r1_r < r2_l || r1_l > r2_r || r1_t < r2_b || r1_b > r2_t)

    o := "False"
    if overlap {
      o = "True"
    }
    fmt.Println(o)

  }
}

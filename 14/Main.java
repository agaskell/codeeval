import java.lang.StringBuilder;
import java.io.*;
import java.util.*;


public class Main {
  static List<String> wordList;

  public static void main (String[] args) throws FileNotFoundException, IOException {
    File file = new File(args[0]);
    BufferedReader in = new BufferedReader(new FileReader(file));
    String line;

    while ((line = in.readLine()) != null) {
      wordList = new ArrayList<String>();
      displayPermutation("", line);

      Collections.sort(wordList);

      boolean addComma = false;
      for (String s : wordList) {
        if(addComma) {
          System.out.print(",");
        }
        addComma = true;
        System.out.print(s);
      }
      System.out.println("");
    }
  }

  public static void displayPermutation(String s1, String s2) {
    if(s2.length() == 0) {
      wordList.add(s1.trim());
    }

    for(int i = 0; i < s2.length(); i++) {
      StringBuilder sb = new StringBuilder(s2);
      sb.deleteCharAt(i);
      displayPermutation(s1 + s2.charAt(i), sb.toString());
    }
  }
}

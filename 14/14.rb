def do_perm(s1, s2, words)
  words.push(s1.strip) if s2.empty?

  0.upto(s2.length - 1) do |i|
    s = s2.clone
    s.slice! i
    do_perm(s1 + s2[i], s, words)
  end
end

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?
  words = []
  do_perm "", line, words
  puts words.sort.join(",")
end

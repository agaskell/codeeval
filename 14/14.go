package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strings"
import "sort"

func Perm(s1 string, s2 string, list *[]string) {
  if len(s2) == 0 {
    *list = append(*list, strings.TrimSpace(s1))
  }

  for i := range s2 {
    Perm(s1 + string(s2[i]), s2[0:i] + s2[i+1:], list)
  }
}

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    s := scanner.Text()
    wordList := make([]string, 0);
    Perm("", s, &wordList)
    sort.Strings(wordList)
    fmt.Println(strings.Join(wordList, ","))
  }
}

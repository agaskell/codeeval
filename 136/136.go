package main

import "fmt"
import "log"
import "bufio"
import "os"
import s "strings"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  prev := -1

  for scanner.Scan() {
    line := scanner.Text()
    moveLocation := s.Index(line, "C")
    if(moveLocation == -1) {
      moveLocation = s.Index(line, "_")
    }

    move := "|"
    if(prev != -1) {
      if prev < moveLocation { move = "\\" }
      if prev > moveLocation { move = "/" }
    }

    fmt.Println(line[:moveLocation] + move + line[moveLocation+1:])
    prev = moveLocation
  }
}

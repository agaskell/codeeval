def numeric?(lookAhead)
  lookAhead =~ /[[:digit:]]/
end

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  map = {
    "a" => "0",
    "b" => "1",
    "c" => "2",
    "d" => "3",
    "e" => "4",
    "f" => "5",
    "g" => "6",
    "h" => "7",
    "i" => "8",
    "j" => "9"
  }

  s = ""
  0.upto(line.length - 1) do |i|
    s += line[i] if numeric? line[i]
    s += map[line[i]] if map.keys.include? line[i]
  end

  puts s.empty? ? "NONE" : s
end

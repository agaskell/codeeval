package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strings"
import "strconv"
import "math"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    s := scanner.Text()

    s = strings.Replace(s, ") (", ",", -1)
    s = strings.Replace(s, "(", "", -1)
    s = strings.Replace(s, ")", "", -1)

    ary := strings.Split(s, ",")

    x1, _ := strconv.Atoi(strings.TrimSpace(ary[0]))
    y1, _ := strconv.Atoi(strings.TrimSpace(ary[1]))
    x2, _ := strconv.Atoi(strings.TrimSpace(ary[2]))
    y2, _ := strconv.Atoi(strings.TrimSpace(ary[3]))

    x1 = x1 - x2
    y1 = y1 - y2

    fmt.Println(math.Sqrt(float64(x1 * x1 + y1 * y1)))
  }
}

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  line.gsub!(") (", ",")
  line.gsub!("(", "")
  line.gsub!(")", "")

  x1, y1, x2, y2 = line.split(",").map(&:to_i)

  puts Math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2).to_i

end

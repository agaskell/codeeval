File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  line = line.gsub(":80", "").downcase

  until (l = line =~ /%[a-zA-Z0-9]{2}/).nil?
    s = line[(l + 1), 2]
    line = line.gsub("%" + s, s.scan(/../).map { |c| c.hex.chr }.join)
  end

  a, b = line.split(";")
  puts a == b ? "True" : "False"
end

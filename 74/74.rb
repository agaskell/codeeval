File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  num = line.to_i

  number_of_fives = num / 5
  remainder = num % 5

  number_of_threes = remainder / 3
  remainder = remainder % 3

  puts number_of_fives + number_of_threes + remainder
end

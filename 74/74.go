package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strconv"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    num, _ := strconv.Atoi(scanner.Text())

    number_of_fives := num / 5
    remainder := num % 5

    number_of_threes := remainder / 3
    remainder = remainder % 3

    fmt.Println(number_of_fives + number_of_threes + remainder)
  }
}

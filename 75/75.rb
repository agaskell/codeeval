#don't you EVER ask me how this works, EVER!
File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  n, m = line.split(',').map(&:to_i)

  n = Array(0..(n -1))
  ary = []
  position = -1

  until n.empty?
    position += m

    overlap = position - (n.length - 1)
    position %= n.length if overlap > 0

    ary << n[position]
    n.delete_at(position)
    position -= 1
  end

  puts ary.join(" ")
end

package main

import "fmt"
import "log"
import "bufio"
import "os"
import s "strings"
import "strconv"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  for scanner.Scan() {
    line := s.Split(scanner.Text(), ",")

    n, _ := strconv.Atoi(line[0])
    m, _ := strconv.Atoi(line[1])

    nums := make([]int, n)

    for i := 0; i < n; i++ {
      nums[i] = i
    }

    ary := make([]string, 0)
    position := -1

    for len(nums) > 0 {
      position += m

      l := len(nums)
      overlap := position - (l - 1)
      if overlap > 0 {
        position %= l
      }

      ary = append(ary, strconv.Itoa(nums[position]))
      nums = nums[:position + copy(nums[position:], nums[position+1:])]
      position--;
    }

    fmt.Println(s.Join(ary, " "))
  }
}

using System;
using System.IO;
using System.Collections.Generic;

class Program
{
  static void Main(string[] args)
  {
    using (var reader = File.OpenText(args[0]))
    while (!reader.EndOfStream)
    {
      var ary = reader.ReadLine().Split(' ');
      Console.WriteLine(ary[ary.Length - 2]);
    }
  }
}

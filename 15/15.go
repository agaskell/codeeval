package main

import "fmt"
import "encoding/binary"

func main() {

  bs := make([]byte, 8)
  binary.LittleEndian.PutUint64(bs, 1)
  a, _ := binary.Uvarint(bs)

  s := "BigEndian"
  if a == 1 {
    s = "LittleEndian"
  }

  fmt.Println(s)
}

using System;

class Program
{
    static void Main(string[] args)
    {
        var o = "BigEndian";
        if(BitConverter.IsLittleEndian) {
            o = "LittleEndian";
        }
        Console.WriteLine(o);
    }
}

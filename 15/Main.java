import java.nio.ByteOrder;

public class Main  {
  public static void main (String[] args) {
    String o = "LittleEndian";
    if (ByteOrder.nativeOrder().equals(ByteOrder.BIG_ENDIAN)) {
      System.out.println("BigEndian");
    }
    System.out.println(o);
  }
}

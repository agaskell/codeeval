File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  nums = line.split(',').map(&:to_i)

  max = nums.max

  until nums.empty?
    num = nums.shift

    0.upto(nums.length - 1) do |i|
      candidate = num + nums[0..i].inject(:+)
      max = candidate if max.nil? or max < candidate
    end
  end

  puts max
end

package main

import "fmt"
import "log"
import "bufio"
import "os"
import s "strings"
import "strconv"

func main() {
  file, err := os.Open(os.Args[1])
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)

  for scanner.Scan() {
    line := s.Split(scanner.Text(), ",")

    l := len(line)
    nums := make([]int, len(line))

    var max int
    maxSet := false

    for i := 0; i < l; i++ {
      temp, _ := strconv.Atoi(line[i])

      if !maxSet || max < temp {
        max = temp
        maxSet = true
      }

      nums[i], _ = strconv.Atoi(line[i])
    }

    for i, num := range nums {

      for j := i + 1; j < l; j++ {
        sum := num

        for k := i + 1; k <= j; k++ {
          sum += nums[k]
        }

        if sum > max { max = sum}
      }

    }

    fmt.Println(max)
  }
}

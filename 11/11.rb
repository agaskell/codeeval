class Node
  attr_accessor :parent, :children, :value

  def initialize(value)
    @value = value
    @children = []
  end

  def add(child)
    @children << child
    child.parent = self
    child
  end

  def to_s
    @value
  end
end

n = Node.new(30)
eight = n.add(Node.new(8))
n.add(Node.new(52))
eight.add(Node.new(3))
twenty = eight.add(Node.new(20))
twenty.add(Node.new(10))
twenty.add(Node.new(29))

def build_stack node, number
  t = node
  ary = []

  until number == t.value
    ary << t.value
    if number < t.value
      t = t.children.first
    elsif number > t.value
      t = t.children.last
    end
  end
  ary << number
  ary
end

File.open(ARGV[0]).each_line do |line|
  next if line.strip!.empty?

  a, b = line.split(" ").map(&:to_i)

  ary_a = build_stack n, a
  ary_b = build_stack n, b

  puts (ary_a & ary_b).last
end

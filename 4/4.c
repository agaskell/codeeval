#include <stdio.h>

int isPrime(int val) {
  for(int i = 2; i * i <= val; i++) {
    if (val % i == 0) { return 0; }
  }
  return 1;
}

int main() {

  int count = 0;
  int i = 2;
  int sum = 0;
  while(count != 1000) {
    if (isPrime(i)) {
      count++;
      sum += i;
    }
    i++;
  }

  printf("%d\n", sum);
  return 0;
}

package main

import "fmt"
import "math/big"

func main() {
  total, count, i := 2, 1, 3

  for count < 1000 {
    b := big.NewInt(int64(i))
    if b.ProbablyPrime(2) {
      total += i
      count++
    }
    i += 2
  }
  fmt.Println(total)
}
